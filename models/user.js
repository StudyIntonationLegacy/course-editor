const mongoose = require("mongoose"),
      Schema = mongoose.Schema;

mongoose.set('useFindAndModify', false);

const userSchema = new Schema(
  {
    login: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

userSchema.set("toJSON", {
  virtuals: true
});

module.exports = mongoose.model("User", userSchema);
