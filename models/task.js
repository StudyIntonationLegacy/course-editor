const mongoose = require("mongoose"),
      Schema = mongoose.Schema;

mongoose.set('useFindAndModify', false);

const taskSchema = new Schema(
  {
    lesson: {
      type: Schema.Types.ObjectId,
      ref: "Lesson"
    },
    number: {
      type: Number
    },
    instructions: {
      type: String,
      required: true
    },
    text: {
      type: String
    },
    sound: {
      type: String
    },
    pitch: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

taskSchema.set("toJSON", {
  virtuals: true
});

module.exports = mongoose.model("Task", taskSchema);
