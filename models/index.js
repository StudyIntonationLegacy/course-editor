const Course = require("./course"),
      User = require("./user"),
      Lesson = require("./lesson"),
      Task = require("./task");

module.exports = {
  User,
  Course,
  Lesson,
  Task
};
