// /* eslint-disable */
// process.env.NODE_ENV = "test";

// const chai = require("chai"),
// 			chaiHttp = require("chai-http"),
//       fs = require("fs"),
//       server = require("../app");
// const should = chai.should();
// const models = require("../models"),
// 			util	 = require("./util");

// chai.use(chaiHttp);

// describe('Upload img and sound with calc pitch', () => {

//   const login = "johnDoe",
//   pass  = "conservatism";

// 	let cookie,
// 	userId;

// 	let course1, lesson1_1, task1_1_1; 

// 	let courseLogo, taskSound, taskPitch;

//   before( async () => {
//     try {
//       await models.User.collection.drop();
//     } catch (err) {}
//     try {
//       await models.Course.collection.drop();
//     } catch (err) {}
//     try {
//       await models.Lesson.collection.drop();
//     } catch (err) {}
//     try {
//       await models.Task.collection.drop();
//     } catch (err) {}

//     const res = await chai
//       .request(server)
//       .post("/auth/registration")
//       .type('form')
//       .send({
//         'login': login,
//         'password': pass,
//         'passwordConfirm': pass
//     });
//     cookie = res.headers['set-cookie'].pop().split(';')[0];
//     userId = res.body.id;

//     course1 = await models.Course.create({
//       title: 'title of course 1',
//       description: 'description',
//       logo: 'none',
//       complexity: '1',
//       category: 'none',
//       authors: 'none',
//       owner: userId,
//       published: true
// 		});
		
//     lesson1_1 = await models.Lesson.create({
//       course: course1.id,
//       title: 'title of lesson 1',
//       number: 1,
//       description: 'description',
//       duration: 1
//     });
      
//     task1_1_1 = await models.Task.create({
//       lesson: lesson1_1.id,
//       number:  1,
//       instructions: 'none',
//       text: 'none',
//       sound: 'none',
//       pitch: 'none'
//     });

//   });

//   after( () => {
//     chai
//     .request(server)
//     .get("/auth/logout")
//     .set('Cookie', cookie);
//   });

// 	describe('img', () => {

// 		it('POST  /upload/image (error "Only jpeg and png!" )', done => {
// 			chai
// 			.request(server)
// 			.post('/upload/image')
//       .field('courseId', course1.id)
//       .attach('file', fs.readFileSync('./test/resource/1.txt'), './test/resource/1.txt')
//       .type('form')
// 			.end(async (err, res) => {
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.req.path.should.be.equal('/upload/image');
// 				res.body.ok.should.be.false;
// 				res.body.error.should.be.equal('Only jpeg and png!');				
    
// 				const testCourse1 = await models.Course.findById(course1.id);
// 				testCourse1.logo.should.be.equal('none');
				
// 				done();
// 			});
// 		});

// 		it('POST  /upload/image (error "Nothing found!" )', done => {
// 			chai
// 			.request(server)
// 			.post('/upload/image')
//       .attach('file', fs.readFileSync('./test/resource/image.jpg'), './test/resource/image.jpg')
//       .type('form')
// 			.end(async (err, res) => {
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.req.path.should.be.equal('/upload/image');
// 				res.body.ok.should.be.false;
// 				res.body.error.should.be.equal('Nothing found!');				
    
// 				const testCourse1 = await models.Course.findById(course1.id);
// 				testCourse1.logo.should.be.equal('none');
				
// 				done();
// 			});
// 		});
      
// 		it("POST  /upload/image (error without cookie )", done => {
// 			chai
// 			.request(server)
// 			.post('/upload/image')
//       .field('courseId', course1.id)
//       .attach('file', fs.readFileSync('./test/resource/image.jpg'), './test/resource/image.jpg')
//       .type('form')
// 			.end(async (err, res) => {
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.req.path.should.be.equal('/upload/image');
// 				res.body.ok.should.be.false;
// 				res.body.error.should.be.equal('Invalid auth!');				
    
// 				const testCourse1 = await models.Course.findById(course1.id);
// 				testCourse1.logo.should.be.equal('none');
				
// 				done();
// 			});
// 		});
      
// 		it("POST  /upload/image (Successful )", done => {
// 			chai
// 			.request(server)
// 			.post('/upload/image')
//       .field('courseId', course1.id)
//       .attach('file', fs.readFileSync('./test/resource/image.jpg'), './test/resource/image.jpg')
//       .type('form')
// 			.set('Cookie', cookie)
// 			.end(async (err, res) => {        
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.body.ok.should.be.true;
// 				res.req.path.should.be.equal('/upload/image');
    
// 				const testCourse1 = await models.Course.findById(course1.id);
// 				testCourse1.logo.should.not.equal('none');

// 				courseLogo = testCourse1.logo;
				
// 				done();
// 			});
// 		});
      
// 		it("GET   /uploads/:imagename (Successful )", done => {
// 			chai
// 			.request(server)
// 			.get(`/uploads/${courseLogo}`)
// 			.end(async (err, res) => {   
// 				res.should.have.status(200);
// 				res.type.should.be.equal('image/jpeg');
// 				res.req.path.should.be.equal(`/uploads/${courseLogo}`);				
				
// 				done();
// 			});
// 		});		

//   });
  
//   describe('sound', () => {
    
// 		it('POST  /upload/sound (error "Only mp3 and wav!" )', done => {
// 			chai
// 			.request(server)
// 			.post('/upload/sound')
//       .field('courseId', course1.id)
//       .attach('file', fs.readFileSync('./test/resource/1.txt'), './test/resource/1.txt')
//       .type('form')
// 			.end(async (err, res) => {
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.req.path.should.be.equal('/upload/sound');
// 				res.body.ok.should.be.false;
// 				res.body.error.should.be.equal('Only mp3 and wav!');				
    
//         const testTask1 = await models.Task.findById(task1_1_1.id);
//         testTask1.sound.should.be.equal('none');
//         testTask1.pitch.should.be.equal('none');
				
// 				done();
// 			});
// 		});

// 		it('POST  /upload/sound (error "Nothing found!" )', done => {
// 			chai
// 			.request(server)
// 			.post('/upload/sound')
//       .attach('file', fs.readFileSync('./test/resource/sound.mp3'), './test/resource/sound.mp3')
//       .type('form')
// 			.end(async (err, res) => {
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.req.path.should.be.equal('/upload/sound');
// 				res.body.ok.should.be.false;
// 				res.body.error.should.be.equal('Nothing found!');				
    
//         const testTask1 = await models.Task.findById(task1_1_1.id);
//         testTask1.sound.should.be.equal('none');
//         testTask1.pitch.should.be.equal('none');
				
// 				done();
// 			});
// 		});
      
// 		it("POST  /upload/sound (error without cookie )", done => {
// 			chai
// 			.request(server)
// 			.post('/upload/sound')
//       .field('taskId', task1_1_1.id)
//       .attach('file', fs.readFileSync('./test/resource/sound.mp3'), './test/resource/sound.mp3')
//       .type('form')
// 			.end(async (err, res) => {
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.req.path.should.be.equal('/upload/sound');
// 				res.body.ok.should.be.false;
// 				res.body.error.should.be.equal('Invalid auth!');				
    
//         const testTask1 = await models.Task.findById(task1_1_1.id);
//         testTask1.sound.should.be.equal('none');
//         testTask1.pitch.should.be.equal('none');
				
// 				done();
// 			});
// 		});
      
// 		it("POST  /upload/sound (Successful )", done => {
// 			chai
// 			.request(server)
// 			.post('/upload/sound')
//       .field('taskId', task1_1_1.id)
//       .attach('file', fs.readFileSync('./test/resource/sound.mp3'), './test/resource/sound.mp3')
//       .type('form')
// 			.set('Cookie', cookie)
// 			.end(async (err, res) => {
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.body.ok.should.be.true;
// 				res.req.path.should.be.equal('/upload/sound');
    
//         const testTask1 = await models.Task.findById(task1_1_1.id);
//         testTask1.sound.should.not.equal('none');
// 				testTask1.pitch.should.not.equal('none');

// 				taskSound = testTask1.sound;
// 				taskPitch = testTask1.pitch;
				
// 				done();
// 			});
// 		});
		
// 		it("GET   /upload/sound (error 404 without ID )", done => {
// 			chai
// 			.request(server)
// 			.get('/upload/sound')
// 			.end(async (err, res) => {
// 				res.should.have.status(404);
// 				res.should.be.html;
// 				res.req.path.should.be.equal('/upload/sound');
// 				done();
// 			});
// 		});
		
// 		it("GET   /upload/sound?id=randomid (error 404 with invalid ID )", done => {
// 			const randomId = util.makeid(24);
// 			chai
// 			.request(server)
// 			.get('/upload/sound')
// 			.query({
// 				'id': randomId
// 			})
// 			.end(async (err, res) => {
// 				res.should.have.status(404);
// 				res.should.be.html;
// 				res.req.path.should.be.equal(`/upload/sound?id=${randomId}`);
// 				done();
// 			});
// 		});

// 		it("GET   /upload/sound (Successful )", done => {
// 			chai
// 			.request(server)
// 			.get('/upload/sound')
// 			.query({
// 				'id': task1_1_1.id
// 			})
// 			.end(async (err, res) => {				
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.body.path.should.be.equal(taskSound);
// 				res.req.path.should.be.equal(`/upload/sound?id=${task1_1_1.id}`);
// 				done();
// 			});
// 		});
      
// 		it("GET   /uploads/:soundname (Successful )", done => {
// 			chai
// 			.request(server)
// 			.get(`/uploads/${taskSound}`)
// 			.end(async (err, res) => {   
// 				res.should.have.status(200);
// 				res.type.should.be.equal('audio/mpeg');
// 				res.req.path.should.be.equal(`/uploads/${taskSound}`);				
// 				done();
// 			});
// 		});
		
// 		it("GET   /upload/sound/pitch (error 404 without ID )", done => {
// 			chai
// 			.request(server)
// 			.get('/upload/sound/pitch')
// 			.end(async (err, res) => {
// 				res.should.have.status(404);
// 				res.should.be.html;
// 				res.req.path.should.be.equal('/upload/sound/pitch');
// 				done();
// 			});
// 		});
		
// 		it("GET   /upload/sound/pitch?id=randomid (error 404 with invalid ID )", done => {
// 			const randomId = util.makeid(24);
// 			chai
// 			.request(server)
// 			.get('/upload/sound/pitch')
// 			.query({
// 				'id': randomId
// 			})
// 			.end(async (err, res) => {
// 				res.should.have.status(404);
// 				res.should.be.html;
// 				res.req.path.should.be.equal(`/upload/sound/pitch?id=${randomId}`);
// 				done();
// 			});
// 		});

// 		it("GET   /upload/sound/pitch (Successful )", done => {
// 			chai
// 			.request(server)
// 			.get('/upload/sound/pitch')
// 			.query({
// 				'id': task1_1_1.id
// 			})
// 			.end(async (err, res) => {				
// 				res.should.have.status(200);
// 				res.should.be.json;
// 				res.body.file.should.be.equal(taskPitch);
// 				res.req.path.should.be.equal(`/upload/sound/pitch?id=${task1_1_1.id}`);
// 				done();
// 			});
// 		});
      
// 		it("GET   /uploads/:pitchname (Successful )", done => {
// 			chai
// 			.request(server)
// 			.get(`/uploads/${taskPitch}`)
// 			.end(async (err, res) => {   
// 				res.should.have.status(200);
// 				res.type.should.be.equal('application/octet-stream');
// 				res.req.path.should.be.equal(`/uploads/${taskPitch}`);
// 				done();
// 			});
// 		});		

//   });

// });
