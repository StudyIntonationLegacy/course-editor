// /* eslint-disable */
// process.env.NODE_ENV = "test";

// const chai   = require("chai");
//       chaiHttp = require("chai-http"),
//       server = require("../app");
// const should = chai.should();
// const models = require("../models"),
//       util   = require("./util");

// chai.use(chaiHttp);

// describe('Edit course, lesson, task', () => {

//   const login = "johnDoe",
//         pass  = "conservatism";

//   let cookie,
//       userId;
    
//   let course1, course2,
//       lesson1_1, lesson1_2, lesson2_1, lesson2_2,
//       task1_1_1, task1_1_2, task1_2_1, task1_2_2,
//       task2_1_1, task2_1_2, task2_2_1, task2_2_2;

//   before( async () => {
//     try {
//       await models.User.collection.drop();
//     } catch (err) {}
//     try {
//       await models.Course.collection.drop();
//     } catch (err) {}
//     try {
//       await models.Lesson.collection.drop();
//     } catch (err) {}
//     try {
//       await models.Task.collection.drop();
//     } catch (err) {}

//     const res = await chai
//       .request(server)
//       .post("/auth/registration")
//       .type('form')
//       .send({
//         'login': login,
//         'password': pass,
//         'passwordConfirm': pass
//     });
//     cookie = res.headers['set-cookie'].pop().split(';')[0];
//     userId = res.body.id;

//     course1 = await models.Course.create({
//       title: 'title of course 1',
//       description: 'description',
//       logo: 'none',
//       complexity: '1',
//       category: 'none',
//       authors: 'none',
//       owner: userId,
//       published: true
//     });
    
//     lesson1_1 = await models.Lesson.create({
//       course: course1.id,
//       title: 'title of lesson 1',
//       number: 1,
//       description: 'description',
//       duration: 1
//     });
      
//     task1_1_1 = await models.Task.create({
//       lesson: lesson1_1.id,
//       number:  1,
//       instructions: 'none',
//       text: 'none',
//       sound: 'none',
//       pitch: 'none'
//     });
  
//     task1_1_2 = await models.Task.create({
//       lesson: lesson1_1.id,
//       number:  2,
//       instructions: 'none',
//       text: 'none',
//       sound: 'none',
//       pitch: 'none'
//     });
    
//     lesson1_2 = await models.Lesson.create({
//       course: course1.id,
//       title: 'title of lesson 2',
//       number: 2,
//       description: 'description',
//       duration: 1
//     });
      
//     task1_2_1 = await models.Task.create({
//       lesson: lesson1_2.id,
//       number:  3,
//       instructions: 'none',
//       text: 'none',
//       sound: 'none',
//       pitch: 'none'
//     });
  
//     task1_2_2 = await models.Task.create({
//       lesson: lesson1_2.id,
//       number:  4,
//       instructions: 'none',
//       text: 'none',
//       sound: 'none',
//       pitch: 'none'
//     });

//     course2 = await models.Course.create({
//       title: 'title of course 2',
//       description: 'description',
//       logo: 'none',
//       complexity: 2,
//       category: 'none',
//       authors: 'none',
//       owner: userId,
//       published: true
//     });

//     lesson2_1 = await models.Lesson.create({
//       course: course2.id,
//       title: 'title of lesson 3',
//       number: 3,
//       description: 'description',
//       duration: 1
//     });
    
//     task2_1_1 = await models.Task.create({
//       lesson: lesson2_1.id,
//       number:  5,
//       instructions: 'none',
//       text: 'none',
//       sound: 'none',
//       pitch: 'none'
//     });

//     task2_1_2 = await models.Task.create({
//       lesson: lesson2_1.id,
//       number:  6,
//       instructions: 'none',
//       text: 'none',
//       sound: 'none',
//       pitch: 'none'
//     });

//     lesson2_2 = await models.Lesson.create({
//       course: course2.id,
//       title: 'title of lesson 4',
//       number: 4,
//       description: 'description',
//       duration: 1
//     });
    
//     task2_2_1 = await models.Task.create({
//       lesson: lesson2_2.id,
//       number:  7,
//       instructions: 'none',
//       text: 'none',
//       sound: 'none',
//       pitch: 'none'
//     });

//     task2_2_2 = await models.Task.create({
//       lesson: lesson2_2.id,
//       number:  8,
//       instructions: 'none',
//       text: 'none',
//       sound: 'none',
//       pitch: 'none'
//     });

//   });

//   after( () => {
//     chai
//     .request(server)
//     .get("/auth/logout")
//     .set('Cookie', cookie);
//   });

//   describe('GET - method', () => {

//     describe('course', () => {

//       it('GET    /edit/course/:aaa!!aaaaaaaaa (error 404 with an invalid id )', done => {
//           chai
//           .request(server)
//           .get(`/edit/course/aaa!!aaaaaaaaa`)
//           .end((err, res) => {
//             res.should.have.status(404);
//             res.should.be.html;
//             res.req.path.should.be.equal('/edit/course/aaa!!aaaaaaaaa');
//             done();
//           });
//       });

//       it('GET    /edit/course/:randomid (error 404 with non-existent id )', done => {
//         const randomId = util.makeid(24);
//         chai
//         .request(server)
//         .get(`/edit/course/${randomId}`)
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/edit/course/${randomId}`);
//           done();
//         });
//       });

//       it('GET    /edit/course/:courseId (error without cookie with redirect to "/" )', done => {
//           chai
//           .request(server)
//           .get(`/edit/course/${course2.id}`)
//           .end((err, res) => {
//             res.should.have.status(200);
//             res.should.be.html;
//             res.req.path.should.be.equal("/");
//             done();
//           });
//       });

//       it("GET    /edit/course/:courseId (Successful )", done => {
//           chai
//           .request(server)
//           .get(`/edit/course/${course2.id}`)
//           .set('Cookie', cookie)
//           .end((err, res) => {
//             res.should.have.status(200);
//             res.should.be.html;
//             res.req.path.should.be.equal(`/edit/course/${course2.id}`);
//             done();
//           });
//       });
//     });

//     describe('lesson', () => {

//         it('GET    /edit/lesson/:aaa!!aaa#aaaaaa (error 404 with an invalid id )', done => {
//           chai
//           .request(server)
//           .get(`/edit/lesson/aaa!!aaa#aaaaaa`)
//           .end((err, res) => {
//             res.should.have.status(404);
//             res.should.be.html;
//             done();
//           });
//       });

//       it('GET    /edit/lesson/:randomid (error 404 with non-existent id )', done => {
//           chai
//           .request(server)
//           .get(`/edit/lesson/${util.makeid(24)}`)
//           .end((err, res) => {
//             res.should.have.status(404);
//             res.should.be.html;
//             done();
//           });
//       });

//       it('GET    /edit/lesson/:lessonId (error without cookie with redirect to "/" )', done => {
//           chai
//           .request(server)
//           .get(`/edit/lesson/${lesson1_2.id}`)
//           .end((err, res) => {
//             res.should.have.status(200);
//             res.should.be.html;
//             res.req.path.should.be.equal('/');
//             done();
//           });
//       });

//       it("GET    /edit/lesson/:lessonId (Successful )", done => {
//           chai
//           .request(server)
//           .get(`/edit/lesson/${lesson1_2.id}`)
//           .set('Cookie', cookie)
//           .end((err, res) => {
//             res.should.have.status(200);
//             res.should.be.html;
//             res.req.path.should.be.equal(`/edit/lesson/${lesson1_2.id}`);
//             done();
//           });
//       });
//     });

//     describe('task', () => {

//       it('GET    /edit/task/:aaa!!aaaaaaaaa (error 404 with an invalid id )', done => {
//           chai
//           .request(server)
//           .get(`/edit/task/aaa!!aaaaaaaaa`)
//           .end((err, res) => {
//             res.should.have.status(404);
//             res.should.be.html;
//             done();
//           });
//       });

//       it('GET    /edit/task/:randomid (error 404 with non-existent id )', done => {
//           chai
//           .request(server)
//           .get(`/edit/task/${util.makeid(24)}`)
//           .end((err, res) => {
//             res.should.have.status(404);
//             res.should.be.html;
//             done();
//           });
//       });

//       it('GET    /edit/task/:taskId (error without cookie with redirect to "/" )', done => {
//           chai
//           .request(server)
//           .get(`/edit/task/${task2_1_1.id}`)
//           .end((err, res) => {
//             res.should.have.status(200);
//             res.should.be.html;
//             res.req.path.should.be.equal('/');
//             done();
//           });
//       });

//       it("GET    /edit/task/:taskId (Successful )", done => {
//           chai
//           .request(server)
//           .get(`/edit/task/${task2_1_1.id}`)
//           .set('Cookie', cookie)
//           .end((err, res) => {
//             res.should.have.status(200);
//             res.should.be.html;
//             res.req.path.should.be.equal(`/edit/task/${task2_1_1.id}`);
//             done();
//           });
//       });
//     });
    
//   });
  
//   describe('POST - method', () => {

//     describe('course', () => {

//       it('POST   /edit/course/ (error without cookie with redirect to "/"  )', done => {
//         const titleName = 'title name 462';
//         chai
//         .request(server)
//         .post('/edit/course/')
//         .send({
//           'title': titleName,
//           'id': course1.id
//         })
//         .end(async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal("/");
//           done();
//         });
//       });
    
//       it('POST   /edit/course/ (error 404 with non-existent id )', done => {
//         const titleName = 'title name 756';
//         chai
//         .request(server)
//         .post('/edit/course/')
//         .send({
//           'title': titleName,
//           'id': util.makeid(24)
//         })
//         .end(async (err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//       });
    
//       it('POST   /edit/course/ (Successful )', done => {
//         const titleName = 'title name 125';
//         chai
//         .request(server)
//         .post('/edit/course/')
//         .set('Cookie', cookie)
//         .send({
//           'title': titleName,
//           'id': course1.id
//         })
//         .end(async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;
//           res.ok.should.be.true;
    
//           const testCourse1 = await models.Course.findById(course1.id);
//           testCourse1.title.should.be.equal(titleName);
          
//           done();
//         });
//       });
//     });
    
//     describe('lesson', () => {

//       it('POST   /edit/lesson/ (error 404 with non-existent id )', done => {
//         const titleName = 'title name 235';
//         chai
//         .request(server)
//         .post('/edit/lesson/')
//         .send({
//           'title': titleName,
//           'id': util.makeid(24)
//         })
//         .end(async (err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//       });
    
//       it('POST   /edit/lesson/ (error without cookie with redirect to "/"  )', done => {
//         const titleName = 'title name 262';
//         chai
//         .request(server)
//         .post('/edit/lesson/')
//         .send({
//           'title': titleName,
//           'id': lesson1_2.id
//         })
//         .end(async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal("/");
//           done();
//         });
//       });
    
//       it('POST   /edit/lesson/ (Successful )', done => {
//         const titleName = 'title name 456';
//         chai
//         .request(server)
//         .post('/edit/lesson/')
//         .set('Cookie', cookie)
//         .send({
//           'title': titleName,
//           'id': lesson1_2.id
//         })
//         .end(async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;
//           res.ok.should.be.true;
    
//           const testLesson2 = await models.Lesson.findById(lesson1_2.id);
//           testLesson2.title.should.be.equal(titleName);
          
//           done();
//         });
//       });    
//     });

//     describe('task', () => {

//       it('POST   /edit/task/ (error 404 with non-existent id )', done => {
//         const taskNumber = 182;
//         chai
//         .request(server)
//         .post('/edit/task/')
//         .send({
//           'number': taskNumber,
//           'id': util.makeid(24)
//         })
//         .end(async (err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//       });
    
//       it('POST   /edit/task/ (error without cookie with redirect to "/"  )', done => {
//         const taskNumber = 435;
//         chai
//         .request(server)
//         .post('/edit/task/')
//         .send({
//           'number': taskNumber,
//           'id': task1_2_2.id
//         })
//         .end(async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal("/");
//           done();
//         });
//       });
    
//       it('POST   /edit/task/ (Successful )', done => {
//         const taskNumber = 739;
//         chai
//         .request(server)
//         .post('/edit/task/')
//         .set('Cookie', cookie)
//         .send({
//           'number': taskNumber,
//           'id': task1_2_2.id
//         })
//         .end(async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;
//           res.ok.should.be.true;
    
//           const testTask4 = await models.Task.findById(task1_2_2.id);
//           testTask4.number.should.be.equal(taskNumber);
          
//           done();
//         });
//       });    
//     });
    
//   });

//   describe('DELETE - method', () => {

//     describe('task', () => {

//       it("DELETE /edit/task/ (error 404 without ID )", done => {
//         chai
//         .request(server)
//         .delete(`/edit/task/`)
//         .end(async (err, res) => {      
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//       });
    
//       it('DELETE /edit/task/ (error without cookie with redirect to "/" )', done => {
//         chai
//         .request(server)
//         .delete(`/edit/task/`)
//         .send({
//           'id': task2_2_1.id
//         })
//         .end(async (err, res) => {      
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal("/");      
//           done();
//         });
//       });
    
//       it("DELETE /edit/task/ (Successful )", done => {
//           chai
//           .request(server)
//           .delete(`/edit/task/`)
//           .set('Cookie', cookie)
//           .send({
//             'id': task2_2_1.id
//           })
//           .end(async (err, res) => {      
//             res.should.have.status(200);
//             res.should.be.json;
//             res.body.ok.should.be.true;
    
//             const testCourse1 = await models.Course.findById(course1.id);
//             const testCourse2 = await models.Course.findById(course2.id);
    
//             const testLesson1 = await models.Lesson.findById(lesson1_1.id);
//             const testLesson2 = await models.Lesson.findById(lesson1_2.id);
//             const testLesson3 = await models.Lesson.findById(lesson2_1.id);
//             const testLesson4 = await models.Lesson.findById(lesson2_2.id);
            
//             const testTask1 = await models.Task.findById(task1_1_1.id);
//             const testTask2 = await models.Task.findById(task1_1_2.id);
//             const testTask3 = await models.Task.findById(task1_2_1.id);
//             const testTask4 = await models.Task.findById(task1_2_2.id);
//             const testTask5 = await models.Task.findById(task2_1_1.id);
//             const testTask6 = await models.Task.findById(task2_1_2.id);
//             const testTask7 = await models.Task.findById(task2_2_1.id);
//             const testTask8 = await models.Task.findById(task2_2_2.id);
    
//             should.exist(testCourse1);
//             should.exist(testCourse2);
    
//             should.exist(testLesson1);
//             should.exist(testLesson2);
//             should.exist(testLesson3);
//             should.exist(testLesson4);
    
//             should.exist(testTask1);
//             should.exist(testTask2);
//             should.exist(testTask3);
//             should.exist(testTask4);
//             should.exist(testTask5);
//             should.exist(testTask6);
//             should.not.exist(testTask7);
//             should.exist(testTask8);
    
//             done();
//           });
//       });
//     });
    
//     describe('lesson', () => {
  
//       it("DELETE /edit/lesson/ (error 404 without ID )", done => {
//         chai
//         .request(server)
//         .delete(`/edit/lesson/`)
//         .end(async (err, res) => {      
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//       });
    
//       it('DELETE /edit/lesson/ (error without cookie with redirect to "/" )', done => {
//         chai
//         .request(server)
//         .delete(`/edit/lesson/`)
//         .send({
//           'id': lesson2_2.id
//         })
//         .end(async (err, res) => {      
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal("/");      
//           done();
//         });
//       });
    
    
//       it("DELETE /edit/lesson/ (Successful )", done => {
//           chai
//           .request(server)
//           .delete(`/edit/lesson/`)
//           .set('Cookie', cookie)
//           .send({
//             'id': lesson2_2.id
//           })
//           .end(async (err, res) => {      
//             res.should.have.status(200);
//             res.should.be.json;
//             res.body.ok.should.be.true;
    
//             const testCourse1 = await models.Course.findById(course1.id);
//             const testCourse2 = await models.Course.findById(course2.id);
    
//             const testLesson1 = await models.Lesson.findById(lesson1_1.id);
//             const testLesson2 = await models.Lesson.findById(lesson1_2.id);
//             const testLesson3 = await models.Lesson.findById(lesson2_1.id);
//             const testLesson4 = await models.Lesson.findById(lesson2_2.id);
            
//             const testTask1 = await models.Task.findById(task1_1_1.id);
//             const testTask2 = await models.Task.findById(task1_1_2.id);
//             const testTask3 = await models.Task.findById(task1_2_1.id);
//             const testTask4 = await models.Task.findById(task1_2_2.id);
//             const testTask5 = await models.Task.findById(task2_1_1.id);
//             const testTask6 = await models.Task.findById(task2_1_2.id);
//             const testTask7 = await models.Task.findById(task2_2_1.id);
//             const testTask8 = await models.Task.findById(task2_2_2.id);
    
//             should.exist(testCourse1);
//             should.exist(testCourse2);
    
//             should.exist(testLesson1);
//             should.exist(testLesson2);
//             should.exist(testLesson3);
//             should.not.exist(testLesson4);
    
//             should.exist(testTask1);
//             should.exist(testTask2);
//             should.exist(testTask3);
//             should.exist(testTask4);
//             should.exist(testTask5);
//             should.exist(testTask6);
//             should.not.exist(testTask7);
//             should.not.exist(testTask8);
    
//             done();
//           });
//       });
//     });

//     describe('course', () => {
  
//       it("DELETE /edit/course/ (error 404 without ID )", done => {
//         chai
//         .request(server)
//         .delete(`/edit/course/`)
//         .end(async (err, res) => {      
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//       });
    
//       it('DELETE /edit/course/ (error without cookie with redirect to "/" )', done => {
//         chai
//         .request(server)
//         .delete(`/edit/course/`)
//         .send({
//           'id': course2.id
//         })
//         .end(async (err, res) => {      
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal("/");      
//           done();
//         });
//       });
    
//       it("DELETE /edit/course/ (Successful )", done => {
//           chai
//           .request(server)
//           .delete(`/edit/course/`)
//           .set('Cookie', cookie)
//           .send({
//             'id': course2.id
//           })
//           .end(async (err, res) => {      
//             res.should.have.status(200);
//             res.should.be.json;
//             res.body.ok.should.be.true;
    
//             const testCourse1 = await models.Course.findById(course1.id);
//             const testCourse2 = await models.Course.findById(course2.id);
    
//             const testLesson1 = await models.Lesson.findById(lesson1_1.id);
//             const testLesson2 = await models.Lesson.findById(lesson1_2.id);
//             const testLesson3 = await models.Lesson.findById(lesson2_1.id);
//             const testLesson4 = await models.Lesson.findById(lesson2_2.id);
            
//             const testTask1 = await models.Task.findById(task1_1_1.id);
//             const testTask2 = await models.Task.findById(task1_1_2.id);
//             const testTask3 = await models.Task.findById(task1_2_1.id);
//             const testTask4 = await models.Task.findById(task1_2_2.id);
//             const testTask5 = await models.Task.findById(task2_1_1.id);
//             const testTask6 = await models.Task.findById(task2_1_2.id);
//             const testTask7 = await models.Task.findById(task2_2_1.id);
//             const testTask8 = await models.Task.findById(task2_2_2.id);
    
//             should.exist(testCourse1);
//             should.not.exist(testCourse2);
    
//             should.exist(testLesson1);
//             should.exist(testLesson2);
//             should.not.exist(testLesson3);
//             should.not.exist(testLesson4);
    
//             should.exist(testTask1);
//             should.exist(testTask2);
//             should.exist(testTask3);
//             should.exist(testTask4);
//             should.not.exist(testTask5);
//             should.not.exist(testTask6);
//             should.not.exist(testTask7);
//             should.not.exist(testTask8);
    
//             done();
//           });
//       });
//     });
    
//   });

// });