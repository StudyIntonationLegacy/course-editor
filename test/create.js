// /* eslint-disable */
// process.env.NODE_ENV = "test";

// const chai = require("chai");
//       chaiHttp = require("chai-http"),
//       server = require("../app");
// const should = chai.should();
// const models = require("../models"),
//       util   = require("./util");

// chai.use(chaiHttp);

// describe('Create course, lesson, task', () => {

//   const login = "johnDoe",
//         pass  = "conservatism";

//   let cookie,
//       userId,
//       courseId,
//       lessonId;

//   before( async () => {
//     try {
//       await models.User.collection.drop();
//     } catch (err) {}
//     try {
//       await models.Course.collection.drop();
//     } catch (err) {}
//     try {
//       await models.Lesson.collection.drop();
//     } catch (err) {}
//     try {
//       await models.Task.collection.drop();
//     } catch (err) {}

//     const res = await chai
//       .request(server)
//       .post("/auth/registration")
//       .type('form')
//       .send({
//         'login': login,
//         'password': pass,
//         'passwordConfirm': pass
//     });
//     cookie = res.headers['set-cookie'].pop().split(';')[0];
//     userId = res.body.id;
//   });

//   after( () => {
//     chai
//     .request(server)
//     .get("/auth/logout")
//     .set('Cookie', cookie);
//   });

//   describe('course', () => {

//     it('GET  /create/course (error without cookie with redirect to "/" )', done => {
//       chai
//         .request(server)
//         .get('/create/course')
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/');
//           done();
//         });
//     });

//     it('GET  /create/course (Successful )', done => {
//       chai
//         .request(server)
//         .get('/create/course')
//         .set('Cookie', cookie)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/create/course');
//           done();
//         });
//     });

//     it('POST /create/course (error without cookie with redirect to "/" )', done => {
//       chai
//         .request(server)
//         .post("/create/course")
//         .send({
//           'title': 'lol'
//         })
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal("/");
//           done();
//         });
//     });

//     it('POST /create/course (error "All fields must be filled in!" for description )', done => {
//       chai
//         .request(server)
//         .post("/create/course")
//         .set('Cookie', cookie)
//         .send({
//           'title': 'lol'
//         })
//         .end( async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;
//           res.body.ok.should.be.false;
//           res.body.error.should.be.equal('All fields must be filled in!');
//           res.body.fields.should.have.members([ 'description' ]);

//           done();
//         });
//     });

//     it('POST /create/course (error "All fields must be filled in!" for title )', done => {
//       chai
//         .request(server)
//         .post("/create/course")
//         .set('Cookie', cookie)
//         .send({
//           'description': 'lol'
//         })
//         .end( async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;
//           res.body.ok.should.be.false;
//           res.body.error.should.be.equal('All fields must be filled in!');
//           res.body.fields.should.have.members([ 'title' ]);

//           done();
//         });
//     });

//     it('POST /create/course (error "All fields must be filled in!" for title and description )', done => {
//       chai
//         .request(server)
//         .post("/create/course")
//         .set('Cookie', cookie)
//         .end( async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;
//           res.body.ok.should.be.false;
//           res.body.error.should.be.equal('All fields must be filled in!');
//           res.body.fields.should.have.members([ 'title', 'description' ]);

//           done();
//         });
//     });

//     it("POST /create/course (Successful )", done => {
//       const title = 'Title of the course',
//             description = 'Description of the course';
//       chai
//         .request(server)
//         .post("/create/course")
//         .set('Cookie', cookie)
//         .send({
//           'title': title,
//           'description': description
//         })
//         .end( async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;
//           res.body.ok.should.be.true;  

//           const course = await models.Course.findOne({ title });
//           if (course) {
//             course.title.should.be.equal(title);
//             course.description.should.be.equal(description);
//             course.published.should.be.false;
//             course.logo.should.be.equal('none');
//             course.complexity.should.be.equal('1');
//             course.category.should.be.equal('none');
//             course.authors.should.be.equal('none');
//             course.owner.toString().should.be.eql(userId);

//             courseId = course.id;
//           } else {
//             done(new Error("Course not found"));
//           }

//           done();
//         });
//     });

//     it('GET  /course/:courseId (error 404 id not exist )', done => {
//       const randomid = util.makeid(24);
//       chai
//         .request(server)
//         .get(`/course/${randomid}`)
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/course/${randomid}`);
//           done();
//         });
//     });

//     it('GET  /course/fhdfdf535ysfdhnms (error 404 invalid id )', done => {
//       chai
//         .request(server)
//         .get('/course/fhdfdf535ysfdhnms')
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           res.req.path.should.be.equal('/course/fhdfdf535ysfdhnms');
//           done();
//         });
//     });

//     it('GET  /course/:courseId (error without cookie with redirect to "/" )', done => {
//       chai
//         .request(server)
//         .get(`/course/${courseId}`)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/');
//           done();
//         });
//     });

//     it('GET  /course/:courseId (Successful )', done => {
//       chai
//         .request(server)
//         .get(`/course/${courseId}`)
//         .set('Cookie', cookie)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/course/${courseId}`);
//           done();
//         });
//     });
//   });
  
//   describe('lesson', () => {

//     it('GET  /create/lesson/:courseId (error without cookie with redirect to "/" )', done => {
//       chai
//         .request(server)
//         .get(`/create/lesson/${courseId}`)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/');
//           done();
//         });
//     });
  
//     it('GET  /create/lesson/:aaaa!!!aaaaaa (error 404 with incorrect characters and length )', done => {
//       chai
//         .request(server)
//         .get('/create/lesson/aaaa!!!aaaaaa')
//         .set('Cookie', cookie)
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//     });
      
//     it('GET  /create/lesson/:randomid (error 404 with non-existent id )', done => {
//       chai
//         .request(server)
//         .get(`/create/lesson/${util.makeid(24)}`)
//         .set('Cookie', cookie)
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//     });

//     it('GET  /create/lesson/:courseId (Successful )', done => {
//       chai
//         .request(server)
//         .get(`/create/lesson/${courseId}`)
//         .set('Cookie', cookie)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/create/lesson/${courseId}`);
//           done();
//         });
//     });

//     it('POST /create/lesson (error 404 without id )', done => {
//       chai
//         .request(server)
//         .post("/create/lesson")
//         .send({
//           'title': 'lol',
//           'description': 'lol'
//         })
//         .end( async (err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;        
//           done();
//         });
//     });

//     it('POST /create/lesson (error without cookie with redirect to "/" )', done => {
//       chai
//         .request(server)
//         .post("/create/lesson")
//         .send({
//           'title': 'lol',
//           'description': 'lol',
//           'id': courseId
//         })
//         .end( async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/');
//           done();
//         });
//     });

//     it("POST /create/lesson (Successful )", done => {
//       const title = 'title of the lesson',
//             number = 1,
//             description = 'description of the lesson',
//             duration = 25;

//       chai
//         .request(server)
//         .post("/create/lesson")
//         .set('Cookie', cookie)
//         .send({
//           'title': title,
//           'number': number,
//           'description': description,
//           'duration': duration,
//           'id': courseId
//         })
//         .end( async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;
//           res.body.ok.should.be.true;

//           const lesson = await models.Lesson.findOne({ title });
//           if (lesson) {
//             lesson.title.should.be.equal(title);
//             lesson.number.should.be.equal(number);
//             lesson.description.should.be.equal(description);
//             lesson.duration.should.be.equal(duration);
//             lesson.course.toString().should.be.equal(courseId);

//             lessonId = lesson.id;
//           } else {
//             done(new Error("Lesson not found"));
//           }
          
//           done();
//         });
//     });

//     it('GET  /lesson/:lessonId (error 404 id not exist )', done => {
//       const randomid = util.makeid(24);
//       chai
//         .request(server)
//         .get(`/lesson/${randomid}`)
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/lesson/${randomid}`);
//           done();
//         });
//     });

//     it('GET  /lesson/ere56245yepwqenwf (error 404 invalid id )', done => {
//       chai
//         .request(server)
//         .get('/lesson/ere56245yepwqenwf')
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           res.req.path.should.be.equal('/lesson/ere56245yepwqenwf');
//           done();
//         });
//     });

//     it('GET  /lesson/:lessonId (error without cookie with redirect to "/" )', done => {
//       chai
//         .request(server)
//         .get(`/lesson/${lessonId}`)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/');
//           done();
//         });
//     });

//     it('GET  /lesson/:lessonId (Successful )', done => {
//       chai
//         .request(server)
//         .get(`/lesson/${lessonId}`)
//         .set('Cookie', cookie)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/lesson/${lessonId}`);
//           done();
//         });
//     });  
    
//   });

//   describe('task', () => {

//     it('GET  /crate/task/:lessonId (error without cookie with redirect to "/" )', done => {
//       chai
//         .request(server)
//         .get(`/create/task/${lessonId}`)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/');
//           done();
//         });
//     });

//     it('GET  /crate/task/:lessonId (error 404 with an invalid id  )', done => {
//       chai
//         .request(server)
//         .get('/create/task/aa#!fdsfgsshb64f')
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//     });

//     it('GET  /crate/task/:lessonId (error 404 with non-existent id )', done => {
//       chai
//         .request(server)
//         .get(`/create/task/${util.makeid(24)}`)
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           done();
//         });
//     });

//     it('GET  /crate/task/:lessonId (Successful )', done => {
//       chai
//         .request(server)
//         .get(`/create/task/${lessonId}`)
//         .set('Cookie', cookie)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/create/task/${lessonId}`);
//           done();
//         });
//     });

//     it('POST /create/task (error 404 without id )', done => {
//       const number = 5;
//       chai
//         .request(server)
//         .post('/create/task')
//         .send({
//           'number': number
//         })
//         .end( async (err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           res.req.path.should.be.equal('/create/task');
//           done();
//         });
//     });

//     it('POST /create/task (error without cookie with redirect to "/" )', done => {
//       const number = 5;
//       chai
//         .request(server)
//         .post("/create/task")
//         .send({
//           'number': number,
//           'lessonId': lessonId
//         })
//         .end( async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/');
//           done();
//         });
//     });

//     it('POST /create/task (Successful )', done => {
//       const number = 5;
//       chai
//         .request(server)
//         .post("/create/task")
//         .set('Cookie', cookie)
//         .send({
//           'number': number,
//           'lessonId': lessonId
//         })
//         .end( async (err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;

//           const task = await models.Task.findOne({ number });
//           if (task) {
//             task.text.should.be.equal('none');
//             task.instructions.should.be.equal('none');
//             task.sound.should.be.equal('none');
//             task.pitch.should.be.equal('none');
//             task.lesson.toString().should.be.equal(lessonId);

//             taskId = task.id;
//           } else {
//             done(new Error("Lesson not found"));
//           }
//           done();
//         });
//     });

//     it('GET  /task/:taskId (error 404 id not exist )', done => {
//       const randomid = util.makeid(24);
//       chai
//         .request(server)
//         .get(`/task/${randomid}`)
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/task/${randomid}`);
//           done();
//         });
//     });

//     it('GET  /task/gsdfgdfg3q453tur (error 404 invalid id )', done => {
//       chai
//         .request(server)
//         .get('/task/gsdfgdfg3q453tur')
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           res.req.path.should.be.equal('/task/gsdfgdfg3q453tur');
//           done();
//         });
//     });

//     it('GET  /task/:taskId (error without cookie with redirect to "/" )', done => {
//       chai
//         .request(server)
//         .get(`/task/${taskId}`)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/');
//           done();
//         });
//     });

//     it('GET  /task/:taskId (Successful )', done => {
//       chai
//         .request(server)
//         .get(`/task/${taskId}`)
//         .set('Cookie', cookie)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/task/${taskId}`);
//           done();
//         });
//     });
//   });  

//   describe('json', () => {

//     it('GET  /course/json/gfdso3453j09fjgdfog (error 404 invalid id )', done => {
//       chai
//         .request(server)
//         .get('/course/json/gfdso3453j09fjgdfog')
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;   
//           res.req.path.should.be.equal('/course/json/gfdso3453j09fjgdfog');     
//           done();
//         });
//     });

//     it('GET  /course/json/:courseId (error 404 id not exist )', done => {
//       const randomid = util.makeid(24);
//       chai
//         .request(server)
//         .get(`/course/json/${randomid}`)
//         .end((err, res) => {
//           res.should.have.status(404);
//           res.should.be.html;
//           res.req.path.should.be.equal(`/course/json/${randomid}`);
//           done();
//         });
//     });

//     it('GET  /course/json/:courseId (error redirect without cookie )', done => {
//       chai
//         .request(server)
//         .get(`/course/json/${courseId}`)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.html;
//           res.req.path.should.be.equal('/');
//           done();
//         });
//     });

//     it('GET  /course/json/:courseId (Successful )', done => {
//       chai
//         .request(server)
//         .get(`/course/json/${courseId}`)
//         .set('Cookie', cookie)
//         .end((err, res) => {
//           res.should.have.status(200);
//           res.should.be.json;
//           res.req.path.should.be.equal(`/course/json/${courseId}`);
//           done();
//         });
//     });
//   });


// });