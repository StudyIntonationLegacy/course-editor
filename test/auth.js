// /* eslint-disable */
// process.env.NODE_ENV = "test";

// const chai = require("chai");
//       chaiHttp = require("chai-http"),
//       server = require("../app");
// const should = chai.should();
// const models = require("../models");

// chai.use(chaiHttp);

// describe('Create user and login', () => {

//   const login = "loginOfUser",
//         pass  = "passOfUser";
//   let cookie;

//   before( async () => {
//     try {
//       await models.User.collection.drop();
//     } catch (err) {}
//   });

//   after( () => {
//     chai
//     .request(server)
//     .get("/auth/logout")
//     .set('Cookie', cookie);
//   });

//   it("GET  /auth/registration ", done => {
//     chai
//       .request(server)
//       .get("/auth/registration")
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.should.be.html;
//         done();
//       });
//   });

//   it('GET  /my-courses (error with redirect to "/" ) ', done => {
//     chai
//       .request(server)
//       .get("/my-courses")
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.should.be.html;
//         res.req.path.should.be.equal("/");
//         done();
//       });
//   });

//   it('POST /auth/registration (error: "All fields must be filled in!" for 3 fields)', done => {
//     chai
//       .request(server)
//       .post("/auth/registration")
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.should.be.json;
//         res.body.ok.should.be.false;
//         res.body.error.should.be.equal("All fields must be filled in!");
//         res.body.fields.should.have.members(['login', 'password', 'passwordConfirm']);
//         done();
//       });
//   });

//   it('POST /auth/registration (error: "All fields must be filled in!" for 2 fields)', done => {
//       chai
//       .request(server)
//       .post("/auth/registration")
//       .type('form')
//       .send({
//         'login': login
//       })
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.should.be.json;
//         res.body.ok.should.be.false;
//         res.body.error.should.be.equal("All fields must be filled in!");
//         res.body.fields.should.have.members(['password', 'passwordConfirm']);
//         done();
//       });
//   });

//   it('POST /auth/registration (error: "Login length from 3 to 16 characters!" for login length = 2)', done => {
//     chai
//       .request(server)
//       .post("/auth/registration")
//       .type('form')
//       .send({
//         'login': 'lo',
//         'password': pass,
//         'passwordConfirm': 'ne-lol'
//       })
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.should.be.json;
//         res.body.ok.should.be.false;
//         res.body.error.should.be.equal("Login length from 3 to 16 characters!");
//         res.body.fields.should.have.members(['login']);   
//         done();     
//       });
//   });

//   it('POST /auth/registration (error: "Login length from 3 to 16 characters!" for login length = 17)', done => {
//     chai
//       .request(server)
//       .post("/auth/registration")
//       .type('form')
//       .send({
//         'login': 'dfgjsdlfjgsdlfngdjsfngsdjfn',
//         'password': pass,
//         'passwordConfirm': 'lol'
//       })
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.should.be.json;
//         res.body.ok.should.be.false;
//         res.body.error.should.be.equal("Login length from 3 to 16 characters!");
//         res.body.fields.should.have.members(['login']);   
//         done();     
//       });
//   });
    
//   it('POST /auth/registration (error: "Passwords do not match!")', done => {
//     chai
//       .request(server)
//       .post("/auth/registration")
//       .type('form')
//       .send({
//         'login': login,
//         'password': pass,
//         'passwordConfirm': 'lol'
//       })
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.should.be.json;
//         res.body.ok.should.be.false;
//         res.body.error.should.be.equal("Passwords do not match!");
//         res.body.fields.should.have.members(['password', 'passwordConfirm']);   
//         done();     
//       });
//   });

//   it('POST /auth/registration (Successful creation)', done => {
//     chai
//       .request(server)
//       .post("/auth/registration")
//       .type('form')
//       .send({
//         'login': login,
//         'password': pass,
//         'passwordConfirm': pass
//       })
//       .end(async (err, res) => {
//         res.should.have.status(200);
//         res.should.be.json;
//         res.body.ok.should.be.true;

//         const user = await models.User.findOne({ login });
//         if (user) {
//           user.login.should.be.equal(login);
//           cookie = res.headers['set-cookie'].pop().split(';')[0];
//         } else {
//           done(new Error("User not found"));
//         }

//         done();
//       });
//   });

//   it('POST /auth/registration (error: "The name is already taken!")', done => {
//     chai
//       .request(server)
//       .post("/auth/registration")
//       .type('form')
//       .send({
//         'login': login,
//         'password': pass,
//         'passwordConfirm': pass
//       })
//       .end(async (err, res) => {
//         res.should.have.status(200);
//         res.should.be.json;

//         res.body.ok.should.be.false;
//         res.body.error.should.be.equal('The name is already taken!');
//         res.body.fields.should.have.members(['login']);

//         done();
//       });
//   });

//   it("GET  /my-courses (Successful) ", done => {
//     chai
//       .request(server)
//       .get("/my-courses")
//       .set('Cookie', cookie)
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.req.path.should.be.equal("/my-courses");
//         done();
//       });
//   });

//   it("GET  /auth/logout (Successful) ", done => {
//     chai
//       .request(server)
//       .get("/auth/logout")
//       .set('Cookie', cookie)
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.req.path.should.be.equal("/");

//         done();
//       });
//   });

//   it('GET  /my-courses (error with redirect to "/" ) ', done => {
//     chai
//       .request(server)
//       .get("/my-courses")
//       .set('Cookie', cookie)
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.req.path.should.be.equal("/");
//         done();
//       });
//   });

//   it('POST /auth/login (error with incorrect pass)', done => {
//     chai
//       .request(server)
//       .post("/auth/login")
//       .type('form')
//       .send({
//         'login': login,
//         'password': 'lol'
//       })
//       .end(async (err, res) => {
//         res.should.have.status(200);        
//         res.should.be.json;
//         res.body.ok.should.be.false;

//         res.body.error.should.be.equal('Login or password do not match!');
//         res.body.fields.should.have.members(['login', 'password']);

//         done();
//       });
//   });

//   it('POST /auth/login (error with incorrect login)', done => {
//     chai
//       .request(server)
//       .post("/auth/login")
//       .type('form')
//       .send({
//         'login': 'lol',
//         'password': pass
//       })
//       .end(async (err, res) => {
//         res.should.have.status(200);        
//         res.should.be.json;
//         res.body.ok.should.be.false;

//         res.body.error.should.be.equal('Login or password do not match!');
//         res.body.fields.should.have.members(['login', 'password']);

//         done();
//       });
//   });


//   it('POST /auth/login (error with incorrect login and password)', done => {
//     chai
//       .request(server)
//       .post("/auth/login")
//       .type('form')
//       .send({
//         'login': 'lol',
//         'password': 'lol'
//       })
//       .end(async (err, res) => {
//         res.should.have.status(200);        
//         res.should.be.json;
//         res.body.ok.should.be.false;

//         res.body.error.should.be.equal('Login or password do not match!');
//         res.body.fields.should.have.members(['login', 'password']);

//         done();
//       });
//   });
  
//   it('POST /auth/login (Successful login)', done => {
//     chai
//       .request(server)
//       .post("/auth/login")
//       .type('form')
//       .send({
//         'login': login,
//         'password': pass
//       })
//       .end(async (err, res) => {
//         res.should.have.status(200);        
//         res.should.be.json;
//         res.body.ok.should.be.true;
//         cookie = res.headers['set-cookie'].pop().split(';')[0];

//         done();
//       });
//   });

//   it("GET  /my-courses (Successful) ", done => {
//     chai
//       .request(server)
//       .get("/my-courses")
//       .set('Cookie', cookie)
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.req.path.should.be.equal("/my-courses");
//         done();
//       });
//   });

//   it("GET  /auth/logout (without send cookie) ", done => {
//     chai
//       .request(server)
//       .get("/auth/logout")
//       .end((err, res) => {
//         res.should.have.status(200);
//         res.req.path.should.be.equal("/");
  
//         done();
//       });
//   });
// });



