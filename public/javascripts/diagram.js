
const data = {
    id: $("#taskId").val()
};

function round10(val) {
    return Math.round(val * 10) / 10;
}

const options = {
    interactionModel: {},
    connectSeparatedPoints: true,
    axes: {
        y: {
            drawAxis: false,
        },
        x: {
            axisLabelWidth: 70,
            axisLabelFormatter: function(num, gran) {
              return round10(num);
            }
        }
    },
    valueRange: [0,600],
    xlabel: 'Время (сек)',
    pixelsPerLabel: 80,
    // ticker: {max : 10},
};

$.ajax({
    type: "GET",
    data: data,
    url: "/upload/sound/pitch"
}).done( data => {    
    if (data.err) {
        console.log(data.err);
    } else {
        new Dygraph(document.getElementById("placeholder"), '/uploads/' + data.file, options);
    }    
});
