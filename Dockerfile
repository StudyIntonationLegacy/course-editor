FROM jrottenberg/ffmpeg:3.4

ENV NODE_ENV development
ENV PORT 8080
ENV MONGO_URL mongodb://localhost:27017/course-editor
ENV TEST_MONGO_URL mongodb://localhost:27017/testDB
ENV SESSION_SECRET sdgfsdf3245gndjfg34253n

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install curl -y
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install nodejs -y
RUN apt-get install build-essential -y
RUN apt-get install libfftw3-3 -y
RUN apt-get install libsamplerate0 -y

WORKDIR /course-editor
ADD . .
ADD ./native_pitch_finder/lib/so/libdspcore_desktop.so /usr/lib

RUN npm install node-gyp -g
RUN npm install graceful-fs -g

RUN cd native_pitch_finder && \
    node-gyp configure && \
    node-gyp build

RUN npm install

ENTRYPOINT npm start
