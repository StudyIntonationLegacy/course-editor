const express = require("express"),
      path = require("path"),
      favicon = require("static-favicon"),
      logger = require("morgan"),
      cookieParser = require("cookie-parser"),
      bodyParser = require("body-parser"),
      proxy = require("express-http-proxy");

const index = require("./routes/index"),
      create = require("./routes/create"),
      edit = require("./routes/edit"),
      auth = require("./routes/auth"),
      course = require("./routes/course/course"),
      lesson = require("./routes/course/lesson"),
      task = require("./routes/course/task"),
      upload = require("./routes/upload"),
      myCourses = require("./routes/my"),
      info = require("./routes/info");

const staticAsset = require("static-asset");

const config = require("./config");

const cookieSession = require('cookie-session');

// database
const mongoose = require("mongoose");
mongoose.set("debug", config.DEBUG);
mongoose.connect(
  config.MONGO_URL,
  { useNewUrlParser: true }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", function() {
  if (config.DEBUG) {
    console.log("Connection to database open.");
  }
});

// express
const app = express();

app.use(cookieSession({
  name: 'session',
  keys: [config.SESSION_SECRET]
}));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

if (process.env.NODE_ENV !== 'test') {
  app.use(logger("dev"));
}

app.use(favicon());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser());
app.use(staticAsset(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "public")));
// app.use("/uploads", express.static(path.join(__dirname, config.DESTINATION)));
app.use('/uploads', proxy('scality:8000', {
  proxyReqPathResolver: (req) => {
    return '/uploads' + req.url;
  }
}));

app.use("/", index);
app.use("/create", create);
app.use("/edit", edit);
app.use("/auth", auth);
app.use("/course", course);
app.use("/lesson", lesson);
app.use("/task", task);
app.use("/upload", upload);
app.use("/my", myCourses);
app.use("/info", info);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handlers

// development error handleenvr
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({ 
    message: err.message,
    error: {}
  });
});

module.exports = app;