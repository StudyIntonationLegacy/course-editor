const express = require('express'),
      path = require('path'),
      app = express();

const proxy = require('./src/setupProxy');

app.use(express.static(path.join(__dirname, 'build')));

proxy(app);

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});


app.listen(3000);