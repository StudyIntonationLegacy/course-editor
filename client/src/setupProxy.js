const proxy = require('http-proxy-middleware');

module.exports = function(app) {

    app.use('/api', proxy({ 
      target: 'http://course-editor:8000/',
      // target: 'http://localhost:8000/',
      pathRewrite: {
        '^/api/index' : '',
        '^/api' : ''
      }
    }));

    app.use('/uploads/', proxy({ 
      target: 'http://scality:8000/',
      // target: 'http://localhost:8080/',
    }));
}
