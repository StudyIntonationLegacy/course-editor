const callApi = async (ref, option) => {
  const response = await fetch(`/api${ref}`, option);
  let body = {};
  try {    
    body = await response.json();
  } catch (error) {
    console.log(error);    
  }
  if (response.status !== 200) {
    throw Error(body.message);
  }
  return body;
};

export default {
  callApi
};