import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import AccountIcon from '@material-ui/icons/AccountCircle';
import MainListItems from './ListItems';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Avatar from '@material-ui/core/Avatar';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Box from '@material-ui/core/Box';
import utils from '../utils';
import Snackbar from './Snackbars';
import { Route, Switch } from 'react-router-dom';
import Courses from './Courses';
import MyCourses from './MyCourses';
import CreateCourse from './create/CreateCourse';
import CreateLesson from './create/CreateLesson';
import CreateTask from './create/CreateTask';
import Content from './content/Content';
import NotFound from './NotFound';

const drawerWidth = 225;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  button: {
    margin: theme.spacing(1),
  },
  input: {
    display: 'none',
  },
  avatarDialog: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  paperDialog: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  formDialog: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submitDialog: {
    margin: theme.spacing(3, 0, 2),
  },
  accName: {
    marginRight: theme.spacing(1),
  },
}));

export default function CourseEditor() {
  const classes = useStyles();
  const [openNav, setOpenNav] = React.useState(false);
  const [openSignIn, setOpenSignIn] = React.useState(false);
  const [openSignUp, setOpenSignUp] = React.useState(false);
  const [accName, setAccName] = React.useState(null);
  const [accId, setAccId] = React.useState(null);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [errorLoginSingIn, setErrorLoginSingIn] = React.useState(false);
  const [errorPasswordSingIn, setErrorPasswordSingIn] = React.useState(false);
  const [errorLoginSingUp, setErrorLoginSingUp] = React.useState(false);
  const [errorPasswordSingUp, setErrorPasswordSingUp] = React.useState(false);
  const [errorPasswordConfirmSingUp, setErrorPasswordConfirmSingUp] = React.useState(false);
  const [messageSnackbar, setMessageSnackbar] = React.useState(null);

  const checkLogin = () => {
    return new Promise( (reslove, reject) => {
      utils.callApi('/auth/login')
      .then(res => {
        setAccName(res.ok ? res.login : null);
        setAccId(res.ok ? res.id : null);
        reslove(res.ok);
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
    })
  };

  const handleDrawerOpen = () => {
    setOpenNav(true);
  };
  const handleDrawerClose = () => {
    setOpenNav(false);
  };

  const handleSignInOpen = () => {
    setOpenSignIn(true);
  };
  const handleSignInClose = () => {
    setErrorLoginSingIn(false);
    setErrorPasswordSingIn(false);
    setOpenSignIn(false);
  };
  const handleSignUpOpen = () => {
    setOpenSignIn(false);
    setOpenSignUp(true);
  };
  const handleSignUpClose = () => {
    setOpenSignIn(false);
    setOpenSignUp(false);
  };

  const handleSignInLoginOnFocus = () => {
    setErrorLoginSingIn(false);
  }
  const handleSignInPasswordOnFocus = () => {
    setErrorPasswordSingIn(false);
  }
  const handleSignUpLoginOnFocus = () => {
    setErrorLoginSingUp(false);
  }
  const handleSignUpPasswordOnFocus = () => {
    setErrorPasswordSingUp(false);
  }
  const handleSignUpPasswordConfirmOnFocus = () => {
    setErrorPasswordConfirmSingUp(false);
  }

  const handleAccMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleAccMenuClose = () => {
    setAnchorEl(null);
  };

  const handleSnackbarClose = () => {
    setMessageSnackbar(null);
  };
  
  const SignInSubmit = e => {
    e.preventDefault();

    setErrorLoginSingIn(false);
    setErrorPasswordSingIn(false);

    utils.callApi('/auth/login', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        login: e.target.login.value,
        password: e.target.password.value,
      })
    })
    .then(res => {
      if (!res.ok) {
        if (res.fields.find( el => el === 'login' )) {
          setErrorLoginSingIn(true);
        }
        if (res.fields.find( el => el === 'password' )) {
          setErrorPasswordSingIn(true);
        }
        setMessageSnackbar(res.error);

      } else {
        setOpenSignIn(false);
        checkLogin();
      }

    })
    .catch(err => console.log(err));  
  }

  const SignUpSubmit = e => {
    e.preventDefault();

    setErrorLoginSingUp(false);
    setErrorPasswordSingUp(false);
    setErrorPasswordConfirmSingUp(false);

    utils.callApi('/auth/registration', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        login: e.target.login.value,
        password: e.target.password.value,
        passwordConfirm: e.target.passwordConfirm.value,
      })
    })
    .then(res => {
      if (!res.ok) {
        if (res.fields.find( el => el === 'login' )) {
          setErrorLoginSingUp(true);
        }
        if (res.fields.find( el => el === 'password' )) {
          setErrorPasswordSingUp(true);
        }
        if (res.fields.find( el => el === 'passwordConfirm' )) {
          setErrorPasswordConfirmSingUp(true);
        }
        setMessageSnackbar(res.error);

      } else {
        setOpenSignUp(false);
        checkLogin();
      }
    })
    .catch(err => console.log(err));  
  }

  const handleSignOut = e => {
    handleAccMenuClose();
    utils.callApi('/auth/logout')
    .then(res => {  
      checkLogin();
    })
    .catch(err => {
      console.log(err);
    });
  }

  React.useEffect(() => {
    checkLogin();
  });

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, openNav && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="Open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, openNav && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            CourseEditor
          </Typography>

          { Boolean(accName) ?
            <Box
              component="div"
            >
              <Typography
                display="inline"
                className={classes.accName}
              >
                {accName}
              </Typography>

              <IconButton 
                aria-owns={Boolean(anchorEl) ? 'menu-appbar' : undefined}
                aria-haspopup="true"
                onClick={handleAccMenu}
                color="inherit"
              >
                  <AccountIcon />
                  <ArrowDropDownIcon />
              </IconButton>
            </Box>
            :
            <Button 
              variant="contained" 
              color='secondary'
              className={classes.button}
              onClick={handleSignInOpen}
            >
                sign in
            </Button>
          }

        </Toolbar>
      </AppBar>

      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorEl)}
        onClose={handleAccMenuClose}
      >
        <MenuItem onClick={handleSignOut}>Sign out</MenuItem>
      </Menu>

      <Dialog open={openSignIn} onClose={handleSignInClose} aria-labelledby="form-dialog-title">
        <DialogContent>
          <div className={classes.paperDialog}>
            <Avatar className={classes.avatarDialog}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            <form
              className={classes.formDialog}
              onSubmit={SignInSubmit}
              noValidate
            >
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="login"
                label="Login"
                name="login"
                autoComplete="login"
                autoFocus
                error={errorLoginSingIn}
                onFocus={handleSignInLoginOnFocus}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                error={errorPasswordSingIn}
                onFocus={handleSignInPasswordOnFocus}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submitDialog}
              >
                Sign In
              </Button>

              {Snackbar(messageSnackbar, Boolean(messageSnackbar), handleSnackbarClose)}

              <Link 
                variant="body2"
                onClick={handleSignUpOpen}
              >
                {"Don't have an account? Sign Up"}
              </Link>
            </form>
          </div>
        </DialogContent>
      </Dialog>

      <Dialog open={openSignUp} onClose={handleSignUpClose} aria-labelledby="form-dialog-title">
        <DialogContent>
          <div className={classes.paperDialog}>
            <Avatar className={classes.avatarDialog}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign up
            </Typography>
            <form
              className={classes.formDialog}
              onSubmit={SignUpSubmit}
              noValidate
            >
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="login"
                label="Login"
                name="login"
                autoComplete="off"
                autoFocus
                error={errorLoginSingUp}
                onFocus={handleSignUpLoginOnFocus}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="off"
                error={errorPasswordSingUp}
                onFocus={handleSignUpPasswordOnFocus}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="passwordConfirm"
                label="Password confirm"
                type="password"
                id="passwordConfirm"
                autoComplete="off"
                error={errorPasswordConfirmSingUp}
                onFocus={handleSignUpPasswordConfirmOnFocus}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submitDialog}
              >
                Sign Up
              </Button>

              {Snackbar(messageSnackbar, Boolean(messageSnackbar), handleSnackbarClose)}

            </form>
          </div>
        </DialogContent>
      </Dialog>

      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !openNav && classes.drawerPaperClose),
        }}
        open={openNav}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>
          <MainListItems auth={Boolean(accName)} />
        </List>
      </Drawer>

      <main className={classes.content}>
        <div className={classes.appBarSpacer} />

        <Container maxWidth="lg" className={classes.container}>

          <Switch>
            <Route exact path="/" render={(props) => (
              <Courses {...props} auth={Boolean(accName)} />
            )}/>
            <Route exact path="/my" render={(props) => {
              if (Boolean(accName)) {
                return (<MyCourses {...props} data={Boolean(accName)} />);
              } else {
                return 'Forbidden!';
              }
            }}/>
            <Route exact path="/create/course" render={(props) => {
              if (Boolean(accName)) {
                return (<CreateCourse {...props} />);
              } else {
                return 'Forbidden!';
              }
            }}/>
            <Route exact path="/create/lesson" render={(props) => {
              if (Boolean(accName)) {
                return (<CreateLesson {...props} />);
              } else {
                return 'Forbidden!';
              }
            }}/>
            <Route exact path="/create/task" render={(props) => {
              if (Boolean(accName)) {
                return (<CreateTask {...props} />);
              } else {
                return 'Forbidden!';
              }
            }}/>
            <Route exact path="/content/:type/:id" render={(props) => (
              <Content {...props} accId={accId} />
            )}/>
            <Route exact path="/content/:edit/:type/:id" render={(props) => (
              <Content {...props} accId={accId} />
            )}/>
            <Route component={NotFound} />
          </Switch>
         
        </Container>
      </main>
    </div>
  );
}
