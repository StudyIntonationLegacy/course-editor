import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import utils from '../../utils';
import Snackbar from '../Snackbars';

const useStyles = makeStyles(theme => ({
  title: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(4, 3),
  },
  button: {
    margin: theme.spacing(1),
  },
}));

function CreateTask(props) {
  const classes = useStyles();
  const [errorNumber, setErrorNumber] = React.useState(false);
  const [errorInstructions, setErrorInstructions] = React.useState(false);
  const [errorText, setErrorText] = React.useState(false);
  const [messageSnackbar, setMessageSnackbar] = React.useState(null);

  const handleSubmit = e => {
    e.preventDefault();

    setErrorNumber(false);
    setErrorInstructions(false);
    setErrorText(false);

    utils.callApi('/create/task', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        number: e.target.number.value,
        instructions: e.target.instructions.value,
        text: e.target.text.value,
        lessonId: props.location.state.lessonId,
      })
    })
    .then(res => {
      if (!res.ok) {
        console.log('res');
        
        console.log(res);
        
        // if (res.fields.find( el => el === 'title' )) {
        //   setErrorTitle(true);
        // }
        // if (res.fields.find( el => el === 'description' )) {
        //   setErrorDescription(true);
        // }
        // setMessageSnackbar(res.error);
      } else {  
        props.history.push(`/content/edit/lesson/${res.lessonId}`);
      }
    })
    .catch(err => console.log(err));  
  };

  const handleOnFocusNumber = () => {
    setErrorNumber(false);
  };
  const handleOnFocusInstructions = () => {
    setErrorInstructions(false);
  };
  const handleOnFocusText = () => {
    setErrorText(false);
  };

  const handleSnackbarClose = () => {
    setMessageSnackbar(null);
  };

  return (
    <div className="container">
      <Typography component="h1" variant="h3" color="inherit" noWrap className={classes.title}>
        Create task
      </Typography>

      <Paper className={classes.paper}>

        <div>
          <Typography variant="h5" component="h3">
            This is a sheet of paper.
          </Typography>
          <Typography component="p">
            Paper can be used to build surface or other elements for your application.
          </Typography>
        </div>

        <form
          onSubmit={handleSubmit}
          noValidate
          autoComplete="off"
        >
          <div>
            <TextField
              id="number"
              name="number"
              label="Number"
              margin="normal"
              variant="outlined"
              type="number"
              fullWidth
              error={errorNumber}
              onFocus={handleOnFocusNumber}
            />
          </div>
          <div>
            <TextField
              id="instructions"
              name="instructions"
              label="Instructions"
              margin="normal"
              variant="outlined"
              fullWidth
              error={errorInstructions}
              onFocus={handleOnFocusInstructions}
            />
          </div>
          <div>
            <TextField
              id="text"
              name="text"
              label="Text"
              margin="normal"
              variant="outlined"
              fullWidth
              error={errorText}
              onFocus={handleOnFocusText}
            />
          </div>
          <div>
            <Button 
              type="submit"
              variant="contained"
              color="primary"
              className={classes.button}
            >
              submit
            </Button>
          </div>
        </form>
        
        {Snackbar(messageSnackbar, Boolean(messageSnackbar), handleSnackbarClose)}

      </Paper>
      
    </div>
  );
}

export default CreateTask;