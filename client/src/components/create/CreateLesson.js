import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import { Redirect } from 'react-router';
import utils from '../../utils';
import Snackbar from '../Snackbars';

const useStyles = makeStyles(theme => ({
  title: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(3),
  },
  paper: {
    padding: theme.spacing(4, 3),
  },
  button: {
    margin: theme.spacing(1),
  },
}));

function CreateLesson(props) {
  const classes = useStyles();
  const [errorTitle, setErrorTitle] = React.useState(false);
  const [errorNumber, setErrorNumber] = React.useState(false);
  const [errorDescription, setErrorDescription] = React.useState(false);
  const [errorDuration, setErrorDuration] = React.useState(false);
  const [messageSnackbar, setMessageSnackbar] = React.useState(null);

  const handleSubmit = e => {
    e.preventDefault();

    setErrorTitle(false);
    setErrorNumber(false);
    setErrorDescription(false);
    setErrorDuration(false);

    utils.callApi('/create/lesson', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        title: e.target.title.value,
        number: e.target.number.value,
        description: e.target.description.value,
        duration: e.target.duration.value,
        courseId: props.location.state.courseId,
      })
    })
    .then(res => {
      if (!res.ok) {
        console.log(res);
        
        // if (res.fields.find( el => el === 'title' )) {
        //   setErrorTitle(true);
        // }
        // if (res.fields.find( el => el === 'description' )) {
        //   setErrorDescription(true);
        // }
        // setMessageSnackbar(res.error);
      } else {  
        props.history.push(`/content/edit/course/${props.location.state.courseId}`);
      }
    })
    .catch(err => console.log(err));  
  };

  const handleOnFocusTitle = () => {
    setErrorTitle(false);
  };
  const handleOnFocusNumber = () => {
    setErrorNumber(false);
  };
  const handleOnFocusDescription = () => {
    setErrorDescription(false);
  };
  const handleOnFocusDuration = () => {
    setErrorDuration(false);
  };

  const handleSnackbarClose = () => {
    setMessageSnackbar(null);
  };

  console.log();
  

  return (
    !props.location.state || !props.location.state.courseId ?
      <Redirect to={'/'} />
    :
      <div className="container">
        <Typography component="h1" variant="h3" color="inherit" noWrap className={classes.title}>
          Create lesson
        </Typography>

        <Paper className={classes.paper}>

          <div>
            <Typography variant="h5" component="h3">
              This is a sheet of paper.
            </Typography>
            <Typography component="p">
              Paper can be used to build surface or other elements for your application.
            </Typography>
          </div>

          <form
            onSubmit={handleSubmit}
            noValidate
            autoComplete="off"
          >
            <div>
              <TextField
                id="title"
                label="Title"
                margin="normal"
                variant="outlined"
                fullWidth
                error={errorTitle}
                onFocus={handleOnFocusTitle}
              />
            </div>
            <div>
              <TextField
                id="number"
                name="number"
                label="Number"
                margin="normal"
                variant="outlined"
                type="number"
                fullWidth
                error={errorNumber}
                onFocus={handleOnFocusNumber}
              />
            </div>
            <div>
              <TextField
                id="description"
                label="Description"
                multiline
                rows="5"
                margin="normal"
                variant="outlined"
                fullWidth
                error={errorDescription}
                onFocus={handleOnFocusDescription}
              />
            </div>
            <div>
              <TextField
                id="duration"
                name="duration"
                label="Duration"
                margin="normal"
                variant="outlined"
                type="number"
                fullWidth
                error={errorDuration}
                onFocus={handleOnFocusDuration}
              />
            </div>
            <div>
              <Button 
                type="submit"
                variant="contained"
                color="primary"
                className={classes.button}
              >
                submit
              </Button>
            </div>
          </form>
          
          {Snackbar(messageSnackbar, Boolean(messageSnackbar), handleSnackbarClose)}

        </Paper>
        
      </div>
  );
}

export default CreateLesson;