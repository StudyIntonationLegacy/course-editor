import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ExitToApp';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { Redirect } from 'react-router';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import utils from '../../utils';

const styles = theme => ({
  title: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  edit: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(1),
  },
  content: {
    margin: theme.spacing(4, 3),
  },
  logo: {
    width: '300px',
  },
  point: {
    marginBottom: theme.spacing(2),
  },
  img: {
    marginTop: theme.spacing(2),
  },
  upload: {
    marginTop: theme.spacing(3),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    'z-index': 999, 
  },
});

class EditCourse extends Component {
  state = {
    course: null,
  };
  
  componentDidMount() {
    this.getCourse(this.props.match.params.courseId);
  }

  componentWillReceiveProps(nextProps) {
    this.getCourse(nextProps.match.params.courseId);
  }

  handleUpload = e => {
    e.preventDefault();

    const formData = new FormData();

    formData.append('courseId', this.state.course.id);
    formData.append('file', e.target.files[0]);

    utils.callApi('/upload/image', {
      method: 'POST',
      body: formData,
    })
    .then(res => {
      // console.log(res);
      this.getCourse(this.props.match.params.courseId);
    })
    .catch(err => console.log(err));
  }

  getCourse(courseId) {
    utils.callApi(`/course/${courseId}`)
    .then(res => {
      if (!res.ok) {
        this.setState({course: null});
      } else {
        this.setState({course: res.course});
      }
    })
    .catch(err => console.log(err));
  }
  
  handleInputChange(e) {
    const target = e.target;
    this.setState((state) => {
      state.course[target.name] = target.value;
      return state;
    });    
  }

  handleInputUnFocus(e) {
    const target = e.target;
    utils.callApi('/edit/course', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        [target.name]: target.value,
        id: this.state.course.id,
      })
    })
    .then(res => {
      // console.log(res);
    })
    .catch(err => console.log(err));
  }  
  
  // TODO - pain
  handleOnClickBack() {
    setTimeout( ()=>{
      this.props.history.push(`/content/course/${this.state.course.id}`)
    }, 500);
  }

  handlePublish = () => {
    utils.callApi(`/course/publish/${this.state.course.id}`)
    .then(res => {
      // console.log(res);      
    })
    .catch(err => console.log(err));
  }

  render() {
    const classes = this.props.classes;
    const course = this.state.course;
    return (
      <div className="container">
        { Boolean(course) ? 
            this.props.accId !== course.owner ?
              <Redirect to={`/content/course/${course.id}`} />
            :
              <form className={classes.root} autoComplete="off">
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                >
                  <div>
                    <TextField
                      id="title"
                      name="title"
                      label="Title"
                      margin="normal"
                      variant="outlined"
                      className={classes.title}
                      value={course.title}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>

                  <IconButton
                    color="inherit"
                    className={classes.edit}
                    onClick={this.handleOnClickBack.bind(this)}
                  >
                    <BackIcon color="primary" />
                  </IconButton>

                </Grid>

                <div className={classes.content}>
                  <div className={classes.point}>
                    <TextField
                      id="description"
                      name="description"
                      label="Description"
                      multiline
                      rows="5"
                      margin="normal"
                      variant="outlined"
                      fullWidth
                      value={course.description}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>
                  <div className={classes.point}>
                    <TextField
                      id="level"
                      name="complexity"
                      label="Level"
                      margin="normal"
                      variant="outlined"
                      type="number"
                      fullWidth
                      value={course.complexity}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>

                  <div className={classes.point}>
                    <TextField
                      id="category"
                      name="category"
                      label="Category"
                      margin="normal"
                      variant="outlined"
                      fullWidth
                      value={course.category}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>

                  <div className={classes.point}>
                    <TextField
                      id="authors"
                      name="authors"
                      label="Authors"
                      margin="normal"
                      variant="outlined"
                      fullWidth
                      value={course.authors}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>

                  <Button
                    variant="contained"
                    component="label"
                    color="primary"
                    className={classes.upload}
                  >
                    Upload
                    <CloudUploadIcon className={classes.rightIcon} />
                    <input
                      id="file"
                      name="file"
                      type="file"
                      style={{ display: "none" }}
                      onChange={this.handleUpload}
                    />
                  </Button>

                  <div className={classes.img}>
                    <img 
                      src={`/uploads/${course.logo}`}
                      alt={course.logo}
                      className={classes.logo}
                    />
                  </div>
                </div>

                <Link
                  to={{
                    pathname: '/create/lesson',
                    state: {
                      courseId: course.id
                    }
                  }}
                  className={this.props.classes.fab}
                >
                  <Fab
                    size="medium"
                    color="secondary"
                    aria-label="Add" 
                  >
                      <AddIcon />
                  </Fab>
                </Link>

                <Grid
                  container justify = "center"
                >
                  <Button
                    variant="contained"
                    component="label"
                    color="primary"
                    onClick={this.handlePublish}
                  >
                    publish
                  </Button>
                </Grid>
                
              </form>
          :
            'No course'
        }
        
      </div>
    );
  }
}

export default withStyles(styles)(EditCourse);