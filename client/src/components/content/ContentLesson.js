import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import { Link } from 'react-router-dom';
import utils from '../../utils';

const styles = theme => ({
  title: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  edit: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(1),
  },
  content: {
    margin: theme.spacing(4, 3),
  },
  point: {
    marginBottom: theme.spacing(2),
  },
});

class ContentLesson extends Component {
  state = {
    lesson: null,
    owner: null,
    published: null,
  };
  
  componentDidMount() {
    this.getLesson(this.props.match.params.lessonId);
  }

  componentWillReceiveProps(nextProps){
    this.getLesson(nextProps.match.params.lessonId);
  }

  getLesson(lessonId) {
    utils.callApi(`/lesson/${lessonId}`)
    .then(res => {
      if (!res.ok) {
        this.setState({
          lesson: null,
          owner: null,
          published: null,
        });
      } else {
        this.setState({
          lesson: res.lesson,
          owner: res.owner,
          published: res.published,
        });        
      }
    })
    .catch(err => console.log(err));
  }
  
  render() {
    const classes = this.props.classes;
    const lesson = this.state.lesson;
    return (
      <div className="container">
        { Boolean(lesson) ? 
            <div>

              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
              >
                <Typography component="h1" variant="h3" color="inherit" className={classes.title}>
                  {lesson.title}
                </Typography>

                { this.props.accId === this.state.owner ? 
                      <Link 
                        to={`/content/edit/lesson/${lesson.id}`}
                        className={classes.edit}
                      >
                        <IconButton 
                          color="inherit"
                        >
                          <EditIcon color="primary" />
                        </IconButton>
                      </Link>
                    :
                      null
                }
              </Grid>
                          
              <div className={classes.content}>
                <Typography component="div" className={classes.point}>
                  <b>Description:</b> {lesson.description}
                </Typography>
                <Typography component="div" className={classes.point}>
                  <b>Number:</b> {lesson.number}
                </Typography>
                <Typography component="div" className={classes.point}>
                  <b>Duration:</b> {lesson.duration}
                </Typography>
              </div>
              
            </div>
          :
            'No lesson'
        }
        
      </div>
    );
  }
}

export default withStyles(styles)(ContentLesson);