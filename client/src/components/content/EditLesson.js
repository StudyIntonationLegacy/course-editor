import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ExitToApp';
import TextField from '@material-ui/core/TextField';
import { Redirect } from 'react-router';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import utils from '../../utils';

const styles = theme => ({
  title: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  edit: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(1),
  },
  content: {
    margin: theme.spacing(4, 3),
  },
  point: {
    marginBottom: theme.spacing(2),
  },
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    'z-index': 999, 
  },
});

class EditLesson extends Component {
  state = {
    lesson: null,
    owner: null,
    published: null,
  };
  
  componentDidMount() {
    this.getLesson(this.props.match.params.lessonId);
  }

  componentWillReceiveProps(nextProps) {
    this.getLesson(nextProps.match.params.lessonId);
  }

  getLesson(lessonId) {
    utils.callApi(`/lesson/${lessonId}`)
    .then(res => {
      if (!res.ok) {
        this.setState({
          lesson: null,
          owner: null,
          published: null,
        });
      } else {
        this.setState({
          lesson: res.lesson,
          owner: res.owner,
          published: res.published,
        });
      }
    })
    .catch(err => console.log(err));
  }
  
  handleInputChange(e) {
    const target = e.target;
    this.setState((state) => {
      state.lesson[target.name] = target.value;
      return state;
    });    
  }

  handleInputUnFocus(e) {
    const target = e.target;
    utils.callApi('/edit/lesson', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        [target.name]: target.value,
        id: this.state.lesson.id,
      })
    })
    .then(res => {
      // console.log(res);
    })
    .catch(err => console.log(err));
  }  
  
  // TODO - pain
  handleOnClickBack() {
    setTimeout( ()=>{
      this.props.history.push(`/content/lesson/${this.state.lesson.id}`)
    }, 500);
  }

  render() {
    const classes = this.props.classes;
    const lesson = this.state.lesson;
    return (
      <div className="container">
        { Boolean(lesson) ? 
            this.props.accId !== this.state.owner ?
              <Redirect to={`/content/lesson/${lesson.id}`} />
            :
              <form className={classes.root} autoComplete="off">
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                >
                  <div>
                    <TextField
                      id="title"
                      name="title"
                      label="Title"
                      margin="normal"
                      variant="outlined"
                      className={classes.title}
                      value={lesson.title}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>

                  <IconButton
                    color="inherit"
                    className={classes.edit}
                    onClick={this.handleOnClickBack.bind(this)}
                  >
                    <BackIcon color="primary" />
                  </IconButton>

                </Grid>

                <div className={classes.content}>
                  <div className={classes.point}>
                    <TextField
                      id="number"
                      name="number"
                      label="Number"
                      margin="normal"
                      variant="outlined"
                      type="number"
                      fullWidth
                      value={lesson.number}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>
                  <div className={classes.point}>
                    <TextField
                      id="description"
                      name="description"
                      label="Description"
                      multiline
                      rows="5"
                      margin="normal"
                      variant="outlined"
                      fullWidth
                      value={lesson.description}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>

                  <div className={classes.point}>
                    <TextField
                      id="duration"
                      name="duration"
                      label="Duration"
                      margin="normal"
                      variant="outlined"
                      type="number"
                      fullWidth
                      value={lesson.duration}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>

                </div>

                <Link
                  to={{
                    pathname: '/create/task',
                    state: {
                      lessonId: lesson.id
                    }
                  }}
                  className={this.props.classes.fab}
                >
                  <Fab
                    size="medium"
                    color="secondary"
                    aria-label="Add" 
                  >
                      <AddIcon />
                  </Fab>
                </Link>
                
              </form>
          :
            'No lesson'
        }
        
      </div>
    );
  }
}

export default withStyles(styles)(EditLesson);