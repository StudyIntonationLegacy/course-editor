import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import { Link } from 'react-router-dom';
import utils from '../../utils';

const styles = theme => ({
  title: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  edit: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(1),
  },
  content: {
    margin: theme.spacing(4, 3),
  },
  logo: {
    width: '300px',
  },
  point: {
    marginBottom: theme.spacing(2),
  },
  img: {
    marginTop: theme.spacing(3),
  },
});

class ContentCourse extends Component {
  state = {
    course: null,
  };
  
  componentDidMount() {
    this.getCourse(this.props.match.params.courseId);
  }

  componentWillReceiveProps(nextProps){
    this.getCourse(nextProps.match.params.courseId);
  }

  getCourse(courseId) {
    utils.callApi(`/course/${courseId}`)
    .then(res => {
      if (!res.ok) {
        this.setState({course: null});
      } else {
        this.setState({course: res.course});
      }
    })
    .catch(err => console.log(err));
  }

  dateFormat() {
    const date = new Date(this.state.course.createdAt);
    return `${('0' + date.getDate()).slice(-2)}.${('0' +
    (date.getMonth() + 1)).slice(-2)}.${date.getFullYear()}`;
  }
  
  render() {
    const classes = this.props.classes;
    const course = this.state.course;
    return (
      <div className="container">
        { Boolean(course) ? 
            <div>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
              >
                <Typography component="h1" variant="h3" color="inherit" className={classes.title}>
                  {course.title} 
                </Typography>

                { this.props.accId === course.owner ? 
                    <Link 
                      to={`/content/edit/course/${course.id}`}
                      className={classes.edit}
                    >
                      <IconButton 
                        color="inherit"
                      >
                        <EditIcon color="primary" />
                      </IconButton>
                    </Link>
                  :
                    null
                }

              </Grid>

              <div className={classes.content}>
                <Typography component="div" className={classes.point}>
                  <b>Description:</b> {course.description}
                </Typography>
                <Typography component="div" className={classes.point}>
                  <b>Level:</b> {course.complexity}
                </Typography>
                <Typography component="div" className={classes.point}>
                  <b>Category:</b> {course.category}
                </Typography>
                <Typography component="div" className={classes.point}>
                  <b>Authors:</b> {course.authors}
                </Typography>
                <Typography component="div" className={classes.point}>
                  <b>CreatedAt:</b> {this.dateFormat()}
                </Typography>

                <div className={classes.img}>
                  <img 
                    src={`/uploads/${course.logo}`}
                    alt={course.title}
                    className={classes.logo}
                  />
                </div>
              </div>
              
            </div>
          :
            'No course'
        }
        
      </div>
    );
  }
}

export default withStyles(styles)(ContentCourse);