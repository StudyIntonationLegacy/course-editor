import React from 'react';
import Dygraph from 'dygraphs';

class Chart extends React.Component {
  state = {
    data: null
  }

  options = {
    interactionModel: {},
    connectSeparatedPoints: true,
    color: '#3f51b5',
    axes: {
        y: {
            drawAxis: false,
        },
        x: {
            // axisLabelWidth: 70,
            axisLabelFormatter: function(num, gran) {
              return Math.round(num * 10) / 10;
            }
        }
    },
    valueRange: [70,370],
    xlabel: 'Время (сек)',
    pixelsPerLabel: 80,
    // ticker: {max : 10},
  };

  componentDidMount() {
    console.log(this.props.pitch);
    
    new Dygraph(document.getElementById("pitch-chart"), `/uploads/${this.props.pitch}`, this.options);
  }

  componentDidUpdate() {
    if (this.props.pitch) {
      new Dygraph(document.getElementById("pitch-chart"), `/uploads/${this.props.pitch}`, this.options);
    }
  }

  render() {
    return (
      <div
        id="pitch-chart"
        style={{
          width: '100%',
          height: '180px',
        }}
      />
    );
  }

}

export default Chart;
