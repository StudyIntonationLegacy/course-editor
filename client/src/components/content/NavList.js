import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Link from '@material-ui/core/Link';
import ListItem from '@material-ui/core/ListItem';
import Collapse from '@material-ui/core/Collapse';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import { Link as RouterLink } from 'react-router-dom';
import Divider from '@material-ui/core/Divider';
import utils from '../../utils';

function ListItemLink(props) {
  const { to, primary, open, ...other } = props;

  return (
    <li>
      <ListItem button component={RouterLink} to={to} {...other}>
        <ListItemText primary={primary} />
        {open != null ? open ? <ExpandLess /> : <ExpandMore /> : null}
      </ListItem>
    </li>
  );
}

ListItemLink.propTypes = {
  open: PropTypes.bool,
  to: PropTypes.string.isRequired,
};

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  divider: {
    marginTop: theme.spacing(2),
  },
  lists: {
    backgroundColor: theme.palette.background.paper,
    marginTop: theme.spacing(1),
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
});

const LinkRouter = props => <Link {...props} component={RouterLink} />;

class RouterBreadcrumbs extends React.Component {
  state = {
    lessons: null,
    Breadcrumb: {
      course: null,
      lesson: null,
      task: null,
    },
  };

  componentDidMount() {    
    this.getList(this.props.params.type, this.props.params.id);    
  }

  componentWillReceiveProps(nextProps){
    this.getList(nextProps.params.type, nextProps.params.id);
  }

  async getList(type, id) {
    if (type === 'course') {
      await this.setState({
        Breadcrumb: {
          course: id,
          lesson: null,
          task: null,
        },
        open: false,
      });
    } else {
      try {
        const res = await utils.callApi(`/info/${type}/${id}`);
        if (res.ok) {
          if (type === 'lesson') {
            this.setState({
              Breadcrumb: {
                course: res.course.id,
                lesson: res.lesson.id,
                task: null,
              },
            });
          }
          if (type === 'task') {
            this.setState({
              Breadcrumb: {
                course: res.course.id,
                lesson: res.lesson.id,
                task: res.task.id,
              },
            });
          }
        }
      } catch (error) {
        console.log(error);        
      }
    }   
    
    if (this.state.Breadcrumb.course) {
      utils.callApi(`/info/list/${this.state.Breadcrumb.course}`)
      .then(res => {
        if (res.lessons.length > 0) {
          this.setState({
            lessons: res.lessons.map(lesson => {
              return {
                ...lesson,
                open: lesson.id === this.state.Breadcrumb.lesson,
              };
            })
          });
        } else {
          this.setState({lessons: null});
        }
      })
      .catch(err => console.log(err));
    }
  }
  
  render() {
    const { classes } = this.props;
    const lessons = this.state.lessons;

    return (
      <React.Fragment>
        <div className={classes.root}>
          <Breadcrumbs aria-label='Breadcrumb'>
            { this.state.Breadcrumb.lesson ? 
                <LinkRouter
                  color='inherit'
                  to={this.props.params.edit ==='edit' ?
                    `/content/edit/course/${this.state.Breadcrumb.course}`
                    :
                    `/content/course/${this.state.Breadcrumb.course}`}
                >
                  Course
                </LinkRouter>
              :
                <Typography color='textPrimary'>
                  Course
                </Typography>
            }

            { this.state.Breadcrumb.lesson ?
                this.state.Breadcrumb.task ?
                  <LinkRouter
                    color='inherit'
                    to={this.props.params.edit ==='edit' ?
                      `/content/edit/lesson/${this.state.Breadcrumb.lesson}`
                      :
                      `/content/lesson/${this.state.Breadcrumb.lesson}`}
                  >
                    Lesson
                  </LinkRouter>
                :
                  <Typography color='textPrimary'>
                    Lesson
                  </Typography>
              : 
                null
            }
            { this.state.Breadcrumb.task ?
                <Typography color="textPrimary">
                  Task
                </Typography>
              : 
                null
            }
          </Breadcrumbs>

          <Divider className={classes.divider} />

          <div className={classes.lists}>
            <List component='nav'>
              { Boolean(lessons) ?
                  lessons.map(lesson => {                          
                    return (
                      <div key={lesson.id}>
                        <ListItemLink
                          to={this.props.params.edit ==='edit' ?
                          `/content/edit/lesson/${lesson.id}`
                          :
                          `/content/lesson/${lesson.id}`}
                          primary={lesson.title}
                          open={lesson.open}
                        />
                        <Collapse in={lesson.open} timeout='auto' unmountOnExit >
                          <List component='div' disablePadding>
                            {
                              lesson.tasks ? 
                                lesson.tasks.map(task => {
                                  return (
                                    <ListItemLink
                                      to={this.props.params.edit ==='edit' ?
                                      `/content/edit/task/${task.id}`
                                      :
                                      `/content/task/${task.id}`}
                                      primary={`Task ${task.number}`}
                                      key={task.id}
                                      className={classes.nested}
                                    />
                                  );
                                })
                              :
                                null
                            }
                          </List>
                        </Collapse>
                      </div>
                    );
                  })
                :
                  'No lessons'
              }
            </List>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

RouterBreadcrumbs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RouterBreadcrumbs);
