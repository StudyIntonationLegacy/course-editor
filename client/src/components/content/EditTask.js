import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ExitToApp';
import TextField from '@material-ui/core/TextField';
import { Redirect } from 'react-router';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import utils from '../../utils';
import Waveform from './Waveform';

const styles = theme => ({
  title: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  edit: {
    marginTop: theme.spacing(1),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(1),
  },
  content: {
    margin: theme.spacing(4, 3),
  },
  point: {
    marginBottom: theme.spacing(2),
  },
  upload: {
    marginTop: theme.spacing(3),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  waveform: {
    marginTop: theme.spacing(5),
  },
});

class EditTask extends Component {
  state = {
    task: null,
    owner: null,
    published: null,
  };
  
  componentDidMount() {
    this.getTask(this.props.match.params.taskId);
  }

  componentWillReceiveProps(nextProps) {
    this.getTask(nextProps.match.params.taskId);
  }

  handleUpload = e => {
    e.preventDefault();

    const formData = new FormData();

    formData.append('taskId', this.state.task.id);
    formData.append('file', e.target.files[0]);

    utils.callApi('/upload/sound', {
      method: 'POST',
      body: formData,
    })
    .then(res => {
      // console.log(res);
      this.getTask(this.props.match.params.taskId);
    })
    .catch(err => console.log(err));
  }

  getTask(taskId) {
    utils.callApi(`/task/${taskId}`)
    .then(res => {
      if (!res.ok) {
        this.setState({
          task: null,
          owner: null,
          published: null,
        });
      } else {
        this.setState({
          task: res.task,
          owner: res.owner,
          published: res.published,
        });
      }
    })
    .catch(err => console.log(err));
  }
  
  handleInputChange(e) {
    const target = e.target;
    this.setState((state) => {
      state.task[target.name] = target.value;
      return state;
    });    
  }

  handleInputUnFocus(e) {
    const target = e.target;
    utils.callApi('/edit/task', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        [target.name]: target.value,
        id: this.state.task.id,
      })
    })
    .then(res => {
      // console.log(res);
    })
    .catch(err => console.log(err));
  }  

  handleSubmit(e) {
    e.preventDefault();
  }
  
  // TODO - pain
  handleOnClickBack() {
    setTimeout( ()=>{
      this.props.history.push(`/content/task/${this.state.task.id}`)
    }, 500);
  }

  render() {
    const classes = this.props.classes;
    const task = this.state.task;
    return (
      <div className="container">
        { Boolean(task) ? 
            this.props.accId !== this.state.owner ?
              <Redirect to={`/content/task/${task.id}`} />
            :
              <form onSubmit={this.handleSubmit} className={classes.root} autoComplete="off">
                <Grid
                  container
                  direction="row"
                  justify="space-between"
                  alignItems="center"
                >
                  <div>
                    <TextField
                      id="number"
                      name="number"
                      label="Number"
                      margin="normal"
                      variant="outlined"
                      type="number"
                      className={classes.title}
                      value={task.number}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>

                  <IconButton
                    color="inherit"
                    className={classes.edit}
                    onClick={this.handleOnClickBack.bind(this)}
                  >
                    <BackIcon color="primary" />
                  </IconButton>

                </Grid>

                <div className={classes.content}>
                  <div className={classes.point}>
                    <TextField
                      id="instructions"
                      name="instructions"
                      label="Instructions"
                      margin="normal"
                      variant="outlined"
                      fullWidth
                      value={task.instructions}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>
                  <div className={classes.point}>
                    <TextField
                      id="text"
                      name="text"
                      label="Text"
                      margin="normal"
                      variant="outlined"
                      fullWidth
                      value={task.text}
                      onChange={this.handleInputChange.bind(this)}
                      onBlur={this.handleInputUnFocus.bind(this)}
                    />
                  </div>

                  <div className={classes.upload} >
                    <Button
                      variant="contained"
                      component="label"
                      color="primary"
                    >
                      Upload
                      <CloudUploadIcon className={classes.rightIcon} />
                      <input
                        id="file"
                        name="file"
                        type="file"
                        style={{ display: "none" }}
                        onChange={this.handleUpload}
                      />
                    </Button>
                  </div>

                  { task.sound &&  task.pitch && task.sound !== 'none' && task.pitch !== 'none' ? 
                      <div className={classes.waveform}>
                        <Waveform sound={task.sound} pitch={task.pitch} />
                      </div>
                    : 
                      null
                  }

                </div>
              </form>
          :
            'No task'
        }
        
      </div>
    );
  }
}

export default withStyles(styles)(EditTask);