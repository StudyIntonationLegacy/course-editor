import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import WaveSurfer from 'wavesurfer.js';
import Button from '@material-ui/core/Button';
import PlayIcon from '@material-ui/icons/PlayArrow';
import Grid from '@material-ui/core/Grid';
import Chart from './Chart';

const styles = theme => ({
  playbutton: {
    marginTop: theme.spacing(3),
    
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
});

class Waveform extends React.Component {

  componentDidMount() {
    this.wavesurfer = WaveSurfer.create({
      container: '#waveform',
      backend: 'MediaElement',
    });

    this.wavesurfer.load(`/uploads/${this.props.sound}`);
  }

  componentDidUpdate() {
    if(this.props.sound) {
      this.wavesurfer.load(`/uploads/${this.props.sound}`);
    }
  }

  playIt = () => {
    this.wavesurfer.playPause();
  };

  render() {
    const classes = this.props.classes;
    return (
      <div>
        <div
          id="waveform"
        />

        <div>
          <Chart pitch={this.props.pitch} />
        </div>

        <Grid
          container justify = "center"
        >
          <Button
            variant="contained"
            component="label"
            color="primary"
            className={classes.playbutton}
            onClick={this.playIt}
          >
            Play
            <PlayIcon className={classes.rightIcon} />
          </Button>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Waveform);