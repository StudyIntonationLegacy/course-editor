import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import { Route, Switch } from 'react-router-dom';
import NavList from './NavList';
import ContentCourse from './ContentCourse';
import ContentLesson from './ContentLesson';
import ContentTask from './ContentTask';
import EditCourse from './EditCourse';
import EditLesson from './EditLesson';
import EditTask from './EditTask';
import NotFound from '../NotFound';

const styles = theme => ({
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
});

class Content extends Component {
  state = {url: null};
  
  componentDidMount() {
    const url = this.props.match.url;        
    if (!url || !url.match(/(\/edit)?\/(course|lesson|task)\/[0-9a-f]{24}/)) {
      this.setState({url: null});
    } else {
      this.setState({url});
    }
  }

  render() {
    const classes = this.props.classes;
    return (
      Boolean(this.state.url) ?
       <Grid container spacing={3}>
         {/* Course or Lesson or Task*/}
         <Grid item xs={12} md={8} lg={9}>
           <Switch>
            <Route exact path="/content/course/:courseId" render={(props) => (
               <Paper className={classes.paper}>
                 <ContentCourse {...props} accId={this.props.accId} />
               </Paper>
             )} />
            <Route exact path="/content/edit/course/:courseId" render={(props) => (
              <Paper className={classes.paper}>
                <EditCourse {...props} accId={this.props.accId} />
              </Paper>
            )} />
            <Route exact path="/content/lesson/:lessonId" render={(props) => (
              <Paper className={classes.paper}>
                <ContentLesson {...props} accId={this.props.accId} />
              </Paper>
            )} />
            <Route exact path="/content/edit/lesson/:lessonId" render={(props) => (
              <Paper className={classes.paper}>
                <EditLesson {...props} accId={this.props.accId} />
              </Paper>
            )} />
            <Route exact path="/content/task/:taskId" render={(props) => (
              <Paper className={classes.paper}>
                <ContentTask {...props} accId={this.props.accId} />
              </Paper>
            )} />
            <Route exact path="/content/edit/task/:taskId" render={(props) => (
              <Paper className={classes.paper}>
                <EditTask {...props} accId={this.props.accId} />
              </Paper>
            )} />
           </Switch>
         </Grid>
         {/* NavList */}
         <Grid item xs={12} md={4} lg={3}>
           <Paper className={classes.paper}>
             <NavList params={this.props.match.params} accId={this.props.accId} />
           </Paper>
         </Grid>
       </Grid>
     :
       <NotFound />
   );
  }
}

export default withStyles(styles)(Content);
