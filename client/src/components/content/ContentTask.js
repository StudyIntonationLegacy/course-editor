import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import utils from '../../utils';
import Waveform from './Waveform';

const styles = theme => ({
  title: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(1),
  },
  content: {
    margin: theme.spacing(4, 3),
  },
  logo: {
    width: '300px',
  },
  point: {
    marginBottom: theme.spacing(2),
  },
  img: {
    marginTop: theme.spacing(3),
  },
});

class ContentTask extends Component {
  state = {
    task: null,
    owner: null,
    published: null,
  };
  
  componentDidMount() {
    this.getTask(this.props.match.params.taskId);
  }

  componentWillReceiveProps(nextProps){
    this.getTask(nextProps.match.params.taskId);
  }

  getTask(taskId) {
    utils.callApi(`/task/${taskId}`)
    .then(res => {
      if (!res.ok) {
        this.setState({
          task: null,
          owner: null,
          published: null,
        });
      } else {
        this.setState({
          task: res.task,
          owner: res.owner,
          published: res.published,
        });        
      }
    })
    .catch(err => console.log(err));
  }
  
  render() {
    const classes = this.props.classes;
    const task = this.state.task;
    return (
      <div className="container">
        { Boolean(task) ? 
            <div>
              <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
              >
                <Typography component="h1" variant="h3" color="inherit" className={classes.title}>
                  Task {task.number}
                </Typography>

                { this.props.accId === this.state.owner ? 
                      <Link 
                        to={`/content/edit/task/${task.id}`}
                        className={classes.edit}
                      >
                        <IconButton 
                          color="inherit"
                        >
                          <EditIcon color="primary" />
                        </IconButton>
                      </Link>
                    :
                      null
                }
              </Grid>
            
              <div className={this.props.classes.content}>
                <Typography component="div" className={this.props.classes.point}>
                  <b>Instructions:</b> {this.state.task.instructions}
                </Typography>
                <Typography component="div" className={this.props.classes.point}>
                  <b>Text:</b> {this.state.task.text}
                </Typography>
                <Typography component="div" className={this.props.classes.point}>
                  <b>Sound:</b> {this.state.task.sound}
                </Typography>
                <Typography component="div" className={this.props.classes.point}>
                  <b>Pitch:</b> {this.state.task.pitch}
                </Typography>


                { task.sound &&  task.pitch && task.sound !== 'none' && task.pitch !== 'none' ? 
                    <div className={classes.waveform}>
                      <Waveform sound={task.sound} pitch={task.pitch} />
                    </div>
                  : 
                    null
                }

              </div>
              
            </div>
          :
            'No task'
        }
        
      </div>
    );
  }
}

export default withStyles(styles)(ContentTask);