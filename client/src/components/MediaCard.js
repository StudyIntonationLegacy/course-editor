import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  card: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
  link: {
    textDecoration: 'none',
    color: 'rgba(0, 0, 0, 0.87)',
  },
});

function MediaCard(props) {
  const classes = useStyles();
  const course = props.course;  
  const link = `/content/course/${course.id}`;

  return (
    <Grid item xs={12} md={4} lg={3}>
      
      <Card className={classes.card}>

        <Link
          to={link}
          className={classes.link}
        >
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image={`/uploads/${course.logo}`}
              title={course.title}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {course.title}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                Level: {course.complexity}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Link>
        
        <CardActions>
          <Link
            to={link}
            className={classes.link}
          >
            <Button size="small" color="primary">
              More
            </Button>
          </Link>
        </CardActions>
      </Card>

    </Grid>
  );
}

export default MediaCard;