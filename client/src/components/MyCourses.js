import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import Card from './MediaCard';
import utils from '../utils';

const styles = theme => ({
  title: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(4),
    marginLeft: theme.spacing(3),
  },
  fab: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
    'z-index': 999, 
  },
});

class MyCourses extends Component {
  state = {courses: []};

  componentDidMount() {
    utils.callApi('/my')
      .then(res => this.setState({courses: res.courses}))
      .catch(err => console.log(err));
  }

  CoursesCards = () => {    
    if (this.state.courses.length === 0) {
      return (
        <div>
          <p>
            There are no courses, but you should rejoice!
          </p>
        </div>
      )
    } else {
      return (
        <Grid container spacing={4}>
          {this.state.courses.map( course =>
            <Card key={course.id} course={course}/>
          )}
        </Grid>
      )
    }
  }

  render() {
    return (
      <div className="container">
        <Typography component="h1" variant="h3" color="inherit" className={this.props.classes.title}>
          My Courses
        </Typography>

        <Link
          to='/create/course'
          className={this.props.classes.fab}
        >
          <Fab
            size="medium"
            color="secondary"
            aria-label="Add" 
          >
              <AddIcon />
          </Fab>
        </Link>

        <this.CoursesCards className={this.props.classes.lol}/>
        
      </div>
    )
  }
}

export default withStyles(styles)(MyCourses);