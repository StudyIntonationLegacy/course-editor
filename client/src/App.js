import React, {Component} from 'react';
import CourseEditor from './components/CourseEditor';
import { BrowserRouter } from 'react-router-dom';

class App extends Component {

  render() {
    return (
      <div className="App">

        <BrowserRouter>

          <CourseEditor />
          
        </BrowserRouter>
        
      </div>
    );
  }
}

export default App;
