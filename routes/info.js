const express = require('express'),
      router = express.Router(),
      models = require('../models');

router.get('/lesson/:lessonId', async (req, res, next) => {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;

  const lessonId = req.params.lessonId;

  if (!lessonId.match(/^[0-9a-fA-F]{24}$/)) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else {
    const lesson = await models.Lesson.findById(lessonId).select('course');
    if (!lesson) {
      const error = new Error('Not Found');
      error.status = 404;
      next(error);
    } else {
      const course = await models.Course.findById(lesson.course).select('published owner');
      if (!course.published &&
        (!userLogin || !userId || userId != course.owner)) {
        res.json({
          ok: false,
          error: 'Forbidden',
        });
      } else {
        res.json({
          ok: true,
          lesson,
          course,
        });        
      }
    }    
  }
});

router.get('/task/:taskId', async (req, res, next) => {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;

  const taskId = req.params.taskId;

  if (!taskId.match(/^[0-9a-fA-F]{24}$/)) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else {
    const task = await models.Task.findById(taskId).select('lesson');
    if (!task) {
      const error = new Error('Not Found');
      error.status = 404;
      next(error);
    } else {
      const lesson = await models.Lesson.findById(task.lesson).select('course');
      if (!lesson) {
        const error = new Error('Not Found');
        error.status = 404;
        next(error);
      } else {
        const course = await models.Course.findById(lesson.course).select('published owner');
        if (!course.published &&
          (!userLogin || !userId || userId != course.owner)) {
            res.json({
              ok: false,
              error: 'Forbidden',
            });
        } else {
          res.json({
            ok: true,
            task,
            lesson,
            course,
          }); 
        }
      }
    }    
  }
});

router.get('/list/:courseId', async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const courseId = req.params.courseId;

  if (!courseId.match(/^[0-9a-fA-F]{24}$/)) {
    const err = new Error("Not Found");
    err.status = 404;
    next(err);
  } else {
    const course = await models.Course.findById(courseId).select('published owner');
    if (!course) {
      const error = new Error("Not Found");
      error.status = 404;
      next(error);
    } else if (
      !course.published &&
      (!userLogin || !userId || userId != course.owner)
    ) {
      res.json({
        ok: false,
        error: 'Forbidden',
      });
    } else {
      const outputJSON = {
        lessons: []
      };

      const lessons = await models.Lesson.find({ course: course.id }).select('title');
      lessons.forEach(lesson => {
        outputJSON.lessons.push({
          id: lesson.id,
          title: lesson.title,
          tasks: [],
        });
      });

      for (let i = 0, len = lessons.length; i < len; i++) {
        const tasks = await models.Task.find({
          lesson: outputJSON.lessons[i].id
        }).select('number');

        for (let j = 0; j < tasks.length; j++) {
          outputJSON.lessons[i].tasks.push({
            id: tasks[j].id,
            number: tasks[j].number,
          });
        }
      }
    
      res.json(outputJSON);
    }
  }
});

module.exports = router;
