const express = require("express"),
      router = express.Router(),
      models = require("../../models"),
      util = require("../utils");

router.post('/', async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const courseId = req.body.id;
  const course = await models.Course.findById(courseId);
  if (!course) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else if (!userLogin || !userId || userId != course.owner) {
    res.json({
      ok: false,
      error: 'Forbidden',
    });
  } else {
    let {
      title,
      description,
      complexity,
      category,
      authors,
      published
    } = req.body;

    if (!title) {
      title = course.title;
    }
    if (!description) {
      description = course.description;
    }
    if (!complexity) {
      complexity = course.complexity;
    }
    if (!category) {
      category = course.category;
    }
    if (!authors) {
      authors = course.authors;
    }
    if (!published) {
      published = course.published;
    }

    const newCourse = await models.Course.findOneAndUpdate(
      {
        _id: course.id,
        owner: userId
      },
      {
        title,
        description,
        complexity,
        category,
        authors,
        owner: userId,
        published
      },
      { new: true }
    );

    if (!newCourse) {
      res.json({
        ok: false,
        error: 'Something went wrong'
      });
    } else {
      res.json({
        ok: true
      });
    }
  }
});

router.delete('/', async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const courseId = req.body.id;
  const course = await models.Course.findById(courseId);
  if (!course) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else if (!userLogin || !userId || userId != course.owner) {
    res.redirect("/");
  } else {
    var removedTasksOK = true;
    const lessons = await models.Lesson.find({ course: courseId });
    lessons.forEach(async lesson => {
      const tasks = await models.Task.find({ lesson: lesson.id });
      tasks.forEach( task => {
        util.removeUploadsFromS3(task.sound);
        util.removeUploadsFromS3(task.pitch);
      });
      const removedTasks = await models.Task.deleteMany({ lesson: lesson.id });
      removedTasksOK = removedTasksOK && removedTasks.ok;
    });
    const removedLessons = await models.Lesson.deleteMany({ course: course.id });
    util.removeUploadsFromS3(course.logo);
    const removedCourse = await models.Course.findByIdAndRemove(course.id);

    if (!removedCourse || !removedLessons.ok || !removedTasksOK) {
      res.json({
        ok: false,
        error: 'Something went wrong'
      });
    } else {
      res.json({
        ok: true
      });
    }
  }
});

module.exports = router;
