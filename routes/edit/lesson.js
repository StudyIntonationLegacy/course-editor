const express = require("express"),
      router = express.Router(),
      models = require("../../models"),
      util = require("../utils");

router.post('/', async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const lessonId = req.body.id;
  const lesson = await models.Lesson.findById(lessonId);
  if (!lesson) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else {
    const course = await models.Course.findById(lesson.course);

    if (!userLogin || !userId || userId != course.owner) {
      res.json({
        ok: false,
        error: 'Forbidden',
      });
    } else {
      let { title, number, description, duration } = req.body;
  
      if (!title) {
        title = lesson.title;
      }
      if (!number) {
        number = lesson.number;
      }
      if (!description) {
        description = lesson.description;
      }
      if (!duration) {
        duration = lesson.duration;
      }

      const newLesson = await models.Lesson.findOneAndUpdate(
        { _id: lesson.id },
        {
          title,
          number,
          description,
          duration,
        },
        { new: true }
      );
  
      if (!newLesson) {
        res.json({
          ok: false,
          error: 'Something went wrong'
        });
      } else {
        res.json({
          ok: true
        });
      }
    }
  }
});

router.delete('/', async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const lessonId = req.body.id;
  const lesson = await models.Lesson.findById(lessonId);
  if (!lesson) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else {
    const course = await models.Course.findById(lesson.course);
    if (!userLogin || !userId || userId != course.owner) {
      res.redirect("/");
    } else {
      const tasks = await models.Task.find({ lesson: lesson.id });
        tasks.forEach( task => {
          util.removeUploadsFromS3(task.sound);
          util.removeUploadsFromS3(task.pitch);
      });
      const removedTasks = await models.Task.deleteMany({ lesson: lesson.id });
      const removedLesson = await models.Lesson.findByIdAndRemove(lesson.id);
  
      if (!removedLesson || !removedTasks.ok) {
        res.json({
          ok: false,
          error: "Something went wrong"
        });
      } else {
        res.json({
          ok: true,
          url: "/edit/course/" + course.id
        });
      }
    }
  }  
});

module.exports = router;
