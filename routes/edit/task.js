const express = require("express"),
      router = express.Router(),
      models = require("../../models"),
      util = require("../utils");

router.post("/", async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const taskId = req.body.id;
  const task = await models.Task.findById(taskId);
  if (!task) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else {
    const lesson = await models.Lesson.findById(task.lesson);
    const course = await models.Course.findById(lesson.course);
  
    if (!userLogin || !userId || userId != course.owner) {
      res.json({
        ok: false,
        error: 'Forbidden',
      });
    } else {
      let { number, instructions, text } = req.body;

      if (!number) {
        number = task.number;
      }
      if (!instructions) {
        instructions = task.instructions;
      }
      if (!text) {
        text = task.text;
      }
  
      const newTask = await models.Task.findOneAndUpdate(
        {
          _id: task.id,
          lesson: lesson.id
        },
        {
          number,
          instructions,
          text
        },
        { new: true }
      );
  
      if (!newTask) {
        res.json({
          ok: false,
          error: "Something went wrong"
        });
      } else {
        res.json({
          ok: true
        });
      }
    }
  }

});

router.delete("/", async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const taskId = req.body.id;
  const task = await models.Task.findById(taskId);  
  if (!task) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
  } else {
    const lesson = await models.Lesson.findById(task.lesson);
    const course = await models.Course.findById(lesson.course);
  
    if (!userLogin || !userId || userId != course.owner) {
      res.redirect("/");
    } else {
      util.removeUploadsFromS3(task.sound);
      util.removeUploadsFromS3(task.pitch);
  
      const removedTask = await models.Task.findByIdAndRemove(task.id);
  
      if (!removedTask) {
        res.json({
          ok: false,
          error: "Something went wrong"
        });
      } else {
        res.json({
          ok: true,
          url: "/edit/lesson/" + lesson.id
        });
      }
    }
  }
});

module.exports = router;
