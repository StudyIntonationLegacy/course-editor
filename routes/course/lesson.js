const express = require("express"),
      router = express.Router(),
      models = require("../../models");

router.get("/:lessonId", async (req, res, next) => {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;

  const lessonId = req.params.lessonId;

  if (!lessonId.match(/^[0-9a-fA-F]{24}$/)) {
    const err = new Error("Not Found");
    err.status = 404;
    next(err);
  } else {
    const lesson = await models.Lesson.findById(lessonId);
    if (!lesson) {
      const error = new Error("Not Found");
      error.status = 404;
      next(error);
    } else {
      const course = await models.Course.findById(lesson.course).select('published owner');
      if (
        !course.published &&
        (!userLogin || !userId || userId != course.owner)
      ) {
        res.json({
          ok: false,
          error: 'Forbidden',
        });
      } else {
        res.json({
          ok: true,
          lesson,
          owner: course.owner,
          published: course.published,
        });
      }
    }    
  }
});

module.exports = router;
