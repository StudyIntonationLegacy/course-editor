const express = require("express"),
      router = express.Router(),
      models = require("../../models");

router.get("/:taskId", async (req, res, next) => {
  const userId = req.session.userId;
  const userLogin = req.session.userLogin;

  const taskId = req.params.taskId;

  if (!taskId.match(/^[0-9a-fA-F]{24}$/)) {
    const err = new Error("Not Found");
    err.status = 404;
    next(err);
  } else {
    const task = await models.Task.findById(taskId);
    if (!task) {
      const error = new Error("Not Found");
      error.status = 404;
      next(error);
    } else {
      const lesson = await models.Lesson.findById(task.lesson).select('course');
      const course = await models.Course.findById(lesson.course).select('owner published');
      if (
        !course.published &&
        (!userLogin || !userId || userId != course.owner)
      ) {
        res.json({
          ok: false,
          error: 'Forbidden',
        });
      } else {
        res.json({
          ok: true,
          task,
          owner: course.owner,
          published: course.published,
        });
      }
    }    
  }
});

module.exports = router;
