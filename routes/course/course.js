const express = require("express"),
      router = express.Router(),
      request = require('request'),
      models = require("../../models");

router.get("/:courseId", async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const courseId = req.params.courseId;

  if (!courseId.match(/^[0-9a-fA-F]{24}$/)) {
    const err = new Error("Not Found");
    err.status = 404;
    next(err);
  } else {
    const course = await models.Course.findById(courseId);
    if (!course) {
      const error = new Error("Not Found");
      error.status = 404;
      next(error);
    } else if (
      !course.published &&
      (!userLogin || !userId || userId != course.owner)
    ) {
      res.json({
        ok: false,
        error: 'Forbidden',
      });
    } else {
      res.json({
        ok: true,
        course,
      });
    }
  }
});

router.get("/publish/:courseId", async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const courseId = req.params.courseId;

  if (!courseId.match(/^[0-9a-fA-F]{24}$/)) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else {
    const course = await models.Course.findById(courseId);

    if (!course) {
      const error = new Error('Not Found');
      error.status = 404;
      next(error);
    } else if (
      !course.published &&
      (!userLogin || !userId || userId != course.owner)
    ) {
      res.json({
        ok: false,
        error: 'Forbidden',
      });
    } else {
      request.post('http://course-packer:8000/course').form({id:courseId});
      res.json({
        ok: true,
        url: `/uploads/${courseId}.zip`,
      })
    }
  }
});

module.exports = router;
