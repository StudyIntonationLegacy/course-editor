const express = require("express"),
      router = express.Router(),
      models = require("../../models");

router.post("/", async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const lessonId = req.body.lessonId;
  const lesson = await models.Lesson.findById(lessonId);
  if (!lesson) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  } else {
    const course = await models.Course.findById(lesson.course);
    if (!userLogin || !userId || userId != course.owner) {
      res.json({
        ok: false,
        error: 'Forbidden!',
      });
    } else {
      const { number, instructions, text} = req.body;
      const task = await models.Task.create({
        lesson: lesson.id,
        number,
        instructions,
        text,
        sound: 'none',
        pitch: 'none'
      });
  
      res.json({
        ok: true,
        courseId: course.id,
        lessonId: lesson.id,
        taskId: task.id,
      });
    }
  }
});

module.exports = router;
