const express = require("express"),
      router = express.Router(),
      models = require("../../models");

router.post("/", async (req, res, next) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  const courseId = req.body.courseId;
  const course = await models.Course.findById(courseId);
  if (!course) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
  } else if (!userLogin || !userId || userId != course.owner) {
    res.json({
      ok: false,
      error: 'Forbidden!',
    });
  } else {
    const { title, number, description, duration } = req.body;
 
    const lesson = await models.Lesson.create({
      course: course.id,
      title,
      number,
      description,
      duration
    });

    res.json({
      ok: true,
      courseId: course.id,
      lessonId: lesson.id,
    });
  }
});

module.exports = router;
