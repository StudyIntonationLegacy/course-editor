const express = require("express"),
      router = express.Router(),
      models = require("../../models");

router.post("/", async (req, res) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  if (!userLogin || !userId) {
    res.json({
      ok: false,
      error: 'Forbidden!',
    });
  } else {
    const {
      title,
      description,
      published
    } = req.body;

    if (!title || !description) {
      const fields = [];
      if (!title) fields.push('title');
      if (!description) fields.push('description');

      res.json({
        ok: false,
        error: 'All fields must be filled in!',
        fields
      });
    } else if (title.length < 3 || title.length > 20 ||
               description.length < 5 || description.length > 500) {
      const fields = [];
      if (title.length < 3 || title.length > 20) fields.push('title');
      if (description.length < 3 || description.length > 500) fields.push('description');
      
      res.json({
        ok: false,
        error: 'Title: [3-20], Description: [3-500]',
        fields,
      });
    } else {
      const course = await models.Course.create({
        title,
        description,
        logo: 'none',
        complexity: '1',
        category: 'none',
        authors: 'none',
        owner: userId,
        published
      });

      res.json({
        ok: true,
        id: course.id,
      });
    }
  }
});

module.exports = router;
