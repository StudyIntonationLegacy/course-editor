const express = require("express"),
      router = express.Router(),
      models = require("../models");

router.get("/", async (req, res) => {
  try {
    const id = req.session.userId;
    const login = req.session.userLogin;

    const courses = await models.Course.find({ published: true }).sort({
      createdAt: -1
    });

    res.json({
      courses,
      user: {
        id,
        login
      }
    });
  } catch (error) {
    throw new Error("Server Error");
  }
});

module.exports = router;
