const express = require("express"),
      router = express.Router(),
      image = require("../routes/upload/image"),
      sound = require("../routes/upload/sound");

router.use("/image", image);
router.use("/sound", sound);

module.exports = router;
