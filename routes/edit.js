const express = require("express"),
      router = express.Router(),
      course = require("./edit/course"),
      lesson = require("./edit/lesson"),
      task = require("./edit/task");

router.use("/course", course);
router.use("/lesson", lesson);
router.use("/task", task);

module.exports = router;
