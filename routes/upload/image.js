const express = require("express"),
      router = express.Router(),
      path = require("path"),
      multer = require("multer"),
      multerS3 = require('multer-s3'),
      AWS = require("aws-sdk"),
      config = require("../../config"),
      models = require("../../models"),
      util = require("../utils");

const s3 = new AWS.S3(config.AWS_PARAM);
const storage = multerS3({
  s3: s3,
  bucket: config.DESTINATION,
  acl: 'public-read',
  contentType: multerS3.AUTO_CONTENT_TYPE,
  key: async (req, file, cb) => {
    const userLogin = req.session.userLogin;
    const userId = req.session.userId;

    const courseId = req.body.courseId;
    const course = await models.Course.findById(courseId);

    if (!course) {
      let err = new Error('Not Found');
      err.code = 'NOCOURSE';
      cb(err);
    } else if (!userLogin || !userId || userId !=
      course.owner) {
      let err = new Error('Is not allowed');
      err.code = 'NOTALLOWED';
      cb(err);
    } else {
      if (course.logo !== 'none') {
        util.removeUploadsFromS3(course.logo);
      }
      const filePath = Date.now() + path.extname(file.originalname);
      await models.Course.findOneAndUpdate(
        {
          _id: course.id,
          owner: userId
        },
        {
          logo: filePath
        },
        { new: true }
      );      
      cb(null, filePath);
    }
  }
});

const upload = multer({
  storage,
  fileFilter: (req, file, cd) => {
    const ext = path.extname(file.originalname);
    if (ext !== '.jpg' && ext !== '.jpeg' && ext !== '.png') {
      const err = new Error('Extention');
      err.code = 'EXTENTION';
      return cd(err);
    }
    cd(null, true);
  },
  limits: {
    fileSize: 3 * 1024 * 1024,
  },
}).single('file');

router.post('/', (req, res) => {
  upload(req, res, err => {    
    let error = '';
    if (err) {
      console.log(err);      
      if (err.code === 'EXTENTION') {
        error = 'Only jpeg and png!';
      }
      if (err.code === 'NOCOURSE') {
        error = 'Nothing found!';
      }
      if (err.code === 'NOTALLOWED') {
        error = 'Invalid auth!';
      }
      if (err.code === 'LIMIT_FILE_SIZE') {
        error = 'Limit file size!';
      }
      if (err.code === 'NoSuchBucket') {
        util.createBucketS3(config.DESTINATION);
        error = 'No such Bucket, pls, try again.';
      }
    }
    res.json({
      ok: !err,
      error
    });
  });
});

module.exports = router;
