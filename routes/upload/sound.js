const express = require("express"),
      router = express.Router(),
      path = require("path"),
      multer = require("multer"),
      fs = require("fs"),
      config = require("../../config"),
      models = require("../../models"),
      pitchFinder = require("../../native_pitch_finder/pitch_finder"),
      util = require("../utils");

const storage = multer.diskStorage({
  destination: (req, file, cd) => {
    cd(null, config.DESTINATION);
  },
  filename: async (req, file, cd) => {
    const userLogin = req.session.userLogin;
    const userId = req.session.userId;

    const taskId = req.body.taskId;
    const task = await models.Task.findById(taskId);
    if (!task) {
      let err = new Error('Not Found');
      err.code = 'NOCOURSE';
      cd(err);
    } else {
      const lesson = await models.Lesson.findById(task.lesson);
      const course = await models.Course.findById(lesson.course);
  
      if (!userLogin || !userId || userId != course.owner) {
        let err = new Error('Is not allowed');
        err.code = 'NOTALLOWED';
        cd(err);
      } else {
        util.removeUploadsFromS3(task.sound);
        util.removeUploadsFromS3(task.pitch);
        
        const filePath = Date.now() + path.extname(file.originalname);
        cd(null, filePath);
  
        file.taskId = taskId;
        file.lessonId = lesson.id;
      }
    }
  }
});

const upload = multer({
  storage,
  fileFilter: (req, file, cd) => {
    const ext = path.extname(file.originalname);
    if (ext !== '.mp3' && ext !== '.wav') {
      const err = new Error('Extention');
      err.code = 'EXTENTION';
      return cd(err);
    }
    cd(null, true);
  }
}).single('file');

router.post("/", (req, res) => {
  try {
    upload( req, res, async err => {
      let error = '';
      if (!err) {        
        pitchFinder.find_pitch('uploads/' + req.file.filename);
        const newTask = await models.Task.findOneAndUpdate(
          {
            _id: req.file.taskId,
            lesson: req.file.lessonId
          },
          {
            sound: req.file.filename,
            pitch: req.file.filename.split('.')[0] + '.pitch'
          },
          { new: true }
        );

        util.putUploadsToS3(newTask.sound, fs.readFileSync(`./uploads/${newTask.sound}`), 'audio/mpeg');
        util.putUploadsToS3(newTask.pitch, fs.readFileSync(`./uploads/${newTask.pitch}`), 'text/csv');
        
        util.removeUploadsFromFolder(newTask.sound);
        util.removeUploadsFromFolder(newTask.pitch);
      } else {
        if (err.code === 'EXTENTION') {
          error = 'Only mp3 and wav!';
        }
        if (err.code === 'NOCOURSE') {
          error = 'Nothing found!';
        }
        if (err.code === 'NOTALLOWED') {
          error = 'Invalid auth!';
        }   
      }
      res.json({
        ok: !err,
        error
      });
    });
  } catch (error) {
    console.error(error);
  }
});

// router.get("/", async (req, res, next) => {
//   const taskId = req.query.id;
//   if (!taskId || !taskId.match(/^[0-9a-fA-F]{24}$/)) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
//   } else {
//     const task = await models.Task.findById(taskId);
//     if (!task) {
//       var err = new Error('Not Found');
//       err.status = 404;
//       next(err);
//     } else {
//       res.json({
//         path: task.sound
//       });
//     }
//   }
// });

// router.get("/pitch", async (req, res, next) => {
//   const taskId = req.query.id;
//   if (!taskId || !taskId.match(/^[0-9a-fA-F]{24}$/)) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
//   } else {
//     const task = await models.Task.findById(taskId);
//     if (!task) {
//       var err = new Error('Not Found');
//       err.status = 404;
//       next(err);
//     } else {
//       res.json({
//         file: task.pitch
//       });
//     }
//   }
// });

module.exports = router;
