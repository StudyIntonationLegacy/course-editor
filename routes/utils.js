const fs  = require("fs"),
      AWS = require('aws-sdk');

const config = require("../config");


const s3 = new AWS.S3(config.AWS_PARAM);

function removeUploadsFromFolder(file) {
    fs.unlink('uploads/' + file, err => {
        if (err) console.log('Nothing to delete');
        console.log('Successfully deleted uploads/' + file);
    });   
}

function createBucketS3(name) {
    s3.createBucket({ Bucket: name }, function (err, data) {
        if (err) console.log(err)
        else console.log(`Successfully create bucket ${name}`);
    });
}

function removeUploadsFromS3(file) {
	const params = {
		Bucket: config.DESTINATION,
		Key: file
    };
	s3.deleteObject(params, function(err, data) {
		if (err) console.log(err)     
		else console.log(`Successfully deleted ${config.DESTINATION}/${file}`);   
	});
}

function putUploadsToS3(Key, Body, ContentType) {
    const params = {
        Bucket: config.DESTINATION,
        ACL: 'public-read',
        Key,
        Body,
        ContentType
    };
    s3.putObject( params , function(err, data) {
        if (err) {
            console.log(err)
        } else {
            console.log(`Successfully uploaded data to ${params.Bucket}/${params.Key}`);
        }
    });
}

module.exports = {
    removeUploadsFromFolder,
    createBucketS3,
    removeUploadsFromS3,
    putUploadsToS3
};
