const express = require("express"),
      router = express.Router(),
      models = require("../models");

router.get("/", async (req, res) => {
  try {
    const userId = req.session.userId;
    const userLogin = req.session.userLogin;

    const courses = await models.Course.find({ owner: userId }).sort({
      createdAt: -1
    });

    if (!userLogin || !userId) {
      res.json({
        ok: false,
        error: 'Forbidden',
      })
    } else {
      res.json({
        ok: true,
        courses,
      });
    }
  } catch (error) {
    throw new Error("Server Error");
  }
});

module.exports = router;
