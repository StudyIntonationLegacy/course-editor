const express = require("express"),
      router = express.Router(),
      course = require("./create/course"),
      lesson = require("./create/lesson"),
      task = require("./create/task");

router.use("/course", course);
router.use("/lesson", lesson);
router.use("/task", task);

module.exports = router;
