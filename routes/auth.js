const express = require("express"),
      router = express.Router(),
      models = require("../models"),
      bcrypt = require("bcrypt-nodejs");

// GET Registration
router.get("/login", (req, res) => {
  const userLogin = req.session.userLogin;
  const userId = req.session.userId;

  res.json({
    ok: userLogin && userId,
    login: userLogin,
    id: userId,
  });
});

// POST Registration
router.post("/registration", async (req, res) => {
  const { login, password, passwordConfirm } = req.body;
  try {
    const user = await models.User.findOne({ login });
    if (!user) {
      if (!login || !password || !passwordConfirm) {
        const fields = [];
        if (!login) fields.push("login");
        if (!password) fields.push("password");
        if (!passwordConfirm) fields.push("passwordConfirm");

        res.json({
          ok: false,
          error: "All fields must be filled in!",
          fields
        });
      } else if (login.length < 3 || login.length > 16) {
        res.json({
          ok: false,
          error: "Login length from 3 to 16 characters!",
          fields: ["login"]
        });
      } else if (password !== passwordConfirm) {
        res.json({
          ok: false,
          error: "Passwords do not match!",
          fields: ["password", "passwordConfirm"]
        });
      } else {
        bcrypt.hash(password, null, null, async function(err, hash) {
          try {
            const newUser = await models.User.create({
              login,
              password: hash
            });

            req.session.userId = newUser.id;
            req.session.userLogin = newUser.login;

            res.json({
              ok: true,
              id: newUser.id
            });
          } catch (err) {
            console.log(err);
            res.json({
              ok: false,
              error: "Error please try again later!"
            });
          }
        });
      }
    } else {
      res.json({
        ok: false,
        error: "The name is already taken!",
        fields: ["login"]
      });
    }
  } catch (error) {
    throw new Error("Server Error");
  }
});

// Login
router.post("/login", async (req, res) => {
  const { login, password } = req.body;

  if (!login || !password) {
    const fields = [];
    if (!login) fields.push("login");
    if (!password) fields.push("password");

    res.json({
      ok: false,
      error: "All fields must be filled in!",
      fields
    });
  } else {
    const user = await models.User.findOne({ login });
    try {
      if (!user) {
        res.json({
          ok: false,
          error: "Login or password do not match!",
          fields: ["login", "password"]
        });
      } else {
        // Load hash from your password DB.
        bcrypt.compare(password, user.password, function(err, result) {
          if (result) {
            req.session.userId = user.id;
            req.session.userLogin = user.login;

            res.json({
              ok: true
            });
          } else {
            res.json({
              ok: false,
              error: "Login or password do not match!",
              fields: ["login", "password"]
            });
          }
        });
      }
    } catch (err) {
      console.log(err);
      res.json({
        ok: false,
        error: "Error please try again later!"
      });
    }
  }
});

router.get("/logout", (req, res) => {
  req.session = null;
  res.json({
    ok: true,
  });
});

module.exports = router;
