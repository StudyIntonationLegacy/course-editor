#ifndef DSP_CORE_H
#define DSP_CORE_H

#include <vector>
#include <string>

/** dspcore.h
 *   A DSP Core public API.
 *   API provides functions for pitch loading and processing.
 */

namespace dspcore {

   /**
    * Enumerates a set of algorithms for pitch detection.
    */
    enum PitchDetectionAlghorithm {
        YIN,
        AMDF 
    };

   /**
    * Loads data from audio file. MP3 and WAV are supported.
    * @param fileName a path to audio file
    * @param audio audio signal
    * @param audioSampleRate audio signal sample rate
    * @return true if there are no errors
    */
    bool loadAudioFile(/* input */
                       const std::string &fileName,
                       /* output */
                       std::vector<float> &audio,
                       float &audioSampleRate);
    
   /**
    * Computes pitch from audio data.
    * @param audio audio signal
    * @param audioSampleRate audio signal sample rate
    * @param algorithm a type of pitch detection alghorithm  
    * @param pitch pitch readings
    * @param confidence pointwise pitch value confidence rate
    * @param pitchSampleRate pitch signal sample rate
    * @return true if there are no errors
    */
    bool detectPitch(/* input: */
                     const std::vector<float> &audio,
                     int audioSampleRate,
                     PitchDetectionAlghorithm algorithm,
                     /* output: */
                     std::vector<float> &pitch,
                     std::vector<float> &confidence,
                     int &pitchSampleRate);
    
    
   /**
    * Smooths pitch for visualisation.
    * @param pitch pitch readings
    * @param confidence pointwise pitch value confidence rate
    * @param pitchSampleRate pitch signal sample rate
    * @param smoothPitch smooth pitch readings
    * @param smoothPitchSampleRate smooth pitch readings sample rate
    * @param leftBorder time reading where pitch begins
    * @param rightBorder time reading where pitch ends
    * @return true if there are no errors
    */
    bool smoothPitch(/* input */
                     std::vector<float> &pitch,
                     std::vector<float> &confidence,
                     int pitchSampleRate,
                     /* output */
                     std::vector<float> &smoothPitch,
                     int &smoothPitchSampleRate,
                     size_t &leftBorder,
                     size_t &rightBorder);

    float dtw(/* input */
              const std::vector<float> &seq1,
              const std::vector<float> &seq2,
              bool modifySignals = true);

}

#endif
