
#include <node.h>
#include "dspcore.h"
#include <iostream>
#include <fstream>
#include <sstream>


void split(const std::string str, std::vector<std::string> &cont, const char divider) {
    std::stringstream ss(str);
    std::string token;
    while (std::getline(ss, token, divider)) {
        cont.push_back(token);
    }
}

void Find_pitch(const v8::FunctionCallbackInfo<v8::Value>& args) {
  v8::Isolate* isolate = v8::Isolate::GetCurrent();
  v8::HandleScope scope(isolate);

  v8::String::Utf8Value param1(args[0]->ToString());

  /* input */
  const std::string fileName = std::string(*param1); 
  /* output */
  std::vector<float> audioData;
  float sampleRate;
  
  dspcore::loadAudioFile(fileName, audioData, sampleRate);

  /* input: */
  const int frameSize = 2048;
  const int hopSize = 128;
  /* output: */
  std::vector<float> allPitches;
  std::vector<float> allConf;
  int pitchSampleRate;

  // dspcore::DSPCore::YIN(audioData, frameSize, hopSize, sampleRate, allPitches, allConf, time);

  dspcore::detectPitch(audioData, sampleRate, dspcore::YIN, allPitches, allConf, pitchSampleRate);

  std::vector<float> smoothPitch;
  int smoothPitchSampleRate;
  size_t leftBorder;
  size_t rightBorder;

  dspcore::smoothPitch(allPitches, allConf, pitchSampleRate, smoothPitch, smoothPitchSampleRate, leftBorder, rightBorder);

  std::vector<std::string> partsOfName;
  split(fileName, partsOfName, '.');

  std::string pitchFileName = partsOfName[0] + ".pitch";

  std::ofstream ofs;
  ofs.open(pitchFileName);

  ofs << "time"  << "," << "pitch";
  // ofs  << std::endl << smoothPitchSampleRate  << "," << smoothPitch[0];
  for (int i = 0; i < smoothPitch.size(); i++) {
    ofs << std::endl << (float)(i+1)/(float)smoothPitchSampleRate  << "," << smoothPitch[i];
  }

  ofs.close();

  args.GetReturnValue().Set(v8::String::NewFromUtf8(isolate, "world"));
}

void Init(v8::Handle<v8::Object> exports) {
  v8::Isolate* isolate = v8::Isolate::GetCurrent();
  exports->Set(v8::String::NewFromUtf8(isolate, "find_pitch"),
      v8::FunctionTemplate::New(isolate, Find_pitch)->GetFunction());
}

NODE_MODULE(find_pitch, Init)